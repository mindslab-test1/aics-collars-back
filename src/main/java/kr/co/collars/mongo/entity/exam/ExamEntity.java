package kr.co.collars.mongo.entity.exam;

import lombok.Data;
import lombok.NonNull;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.List;

@Document(collection = "exam")
@Data
public class ExamEntity {
    @Id
    private String _id;
    @NotNull
    private String type;
    private List<ExamDataEntity> data;
}
