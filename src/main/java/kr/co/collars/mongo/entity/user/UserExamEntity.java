package kr.co.collars.mongo.entity.user;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.List;

@Document(collection = "userExam")
@Data
public class UserExamEntity {
    @Id
    private String _id;
    @NotNull
    private Integer userId;
    @NotNull
    private Integer examId;
    @NotNull
    private String type;
    private List<UserExamDataEntity> data;
}
