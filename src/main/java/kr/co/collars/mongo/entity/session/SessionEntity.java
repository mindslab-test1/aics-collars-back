package kr.co.collars.mongo.entity.session;

import kr.co.collars.api.session.data.json.SessionEntityData;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "session")
@Data
public class SessionEntity {
    @Id
    private String _id;
    private SessionEntityData data;
}
