package kr.co.collars.mongo.entity.user;

import lombok.Data;

@Data
public class UserExamDataEntity {
    private String type;
    private Integer score;
}
