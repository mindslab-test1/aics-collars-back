package kr.co.collars.mongo.entity;

import kr.co.collars.api.twocents.data.json.TwoCentsScript;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "twoCents")
@Data
public class TwoCentsEntity {
    @Id
    private String _id;
    private TwoCentsScript data;
}
