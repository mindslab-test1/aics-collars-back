package kr.co.collars.mongo.entity.evereading;

import kr.co.collars.api.evereading.data.json.EvereadingData;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "evereading")
@Data
public class EvereadingEntity {
    @Id
    private String _id;
    private EvereadingData data;
}
