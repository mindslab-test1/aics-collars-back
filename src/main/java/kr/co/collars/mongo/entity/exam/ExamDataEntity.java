package kr.co.collars.mongo.entity.exam;

import lombok.Data;

import java.util.List;

@Data
public class ExamDataEntity {
    private String type;
    private String question;
    private List<Object> subQuestion;
    private List<Object> answer;
}
