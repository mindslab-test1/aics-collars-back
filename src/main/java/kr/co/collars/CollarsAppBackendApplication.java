package kr.co.collars;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CollarsAppBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(CollarsAppBackendApplication.class, args);
    }

}
