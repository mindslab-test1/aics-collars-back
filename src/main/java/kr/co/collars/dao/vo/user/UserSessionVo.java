package kr.co.collars.dao.vo.user;

import lombok.Data;

import java.util.Date;

@Data
public class UserSessionVo {
    private Integer id;
    private Integer userId;
    private Integer sessionId;
    private String purchaseToken;
    private Date purchaseDate;
    private Date expiryDate;
    private Integer duration;
}
