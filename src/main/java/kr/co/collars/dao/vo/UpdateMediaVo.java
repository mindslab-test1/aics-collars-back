package kr.co.collars.dao.vo;

import lombok.Data;

@Data
public class UpdateMediaVo {
    private Integer id;
    private Integer mediaId;
    private String type;
}
