package kr.co.collars.dao.vo.mongo;

import lombok.Data;

@Data
public class MongoVo {
    private Integer id;
    private String collection;
    private String objectId;
}
