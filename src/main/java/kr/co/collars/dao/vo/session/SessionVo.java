package kr.co.collars.dao.vo.session;

import lombok.Data;

import java.util.Date;

@Data
public class SessionVo {
    private Integer id;
    private String name;
    private Integer video;
    private String price;
    private Integer mongoId;
    private Integer thumbnail;
    private Date createDate;
    private Integer examId;
}
