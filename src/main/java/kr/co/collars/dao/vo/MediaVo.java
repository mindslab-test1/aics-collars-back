package kr.co.collars.dao.vo;

import lombok.Data;

import java.util.Date;

@Data
public class MediaVo {
    private Integer id;
    private String contentType;
    private String url;
    private Date createDate;
}
