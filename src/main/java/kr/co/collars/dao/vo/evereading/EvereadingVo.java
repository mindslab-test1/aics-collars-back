package kr.co.collars.dao.vo.evereading;

import lombok.Data;

import java.util.Date;

@Data
public class EvereadingVo {
    private Integer id;
    private String name;
    private Integer mongoId;
    private Integer sectorKor;
    private Integer sectorEng;
    private Integer lev;
    private Date createDate;
    private Integer thumbnail;
    private Integer introBanner;
    private Integer banner;
}
