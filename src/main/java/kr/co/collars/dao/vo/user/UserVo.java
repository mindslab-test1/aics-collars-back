package kr.co.collars.dao.vo.user;

import lombok.Data;

import java.sql.Date;

@Data
public class UserVo {
    private Integer id;
    private String email;
    private String name;
    private Date signUpDate;
    private Date signInDate;
    private Integer google;
    private Integer naver;
    private Integer kakao;
    private Integer password;
    private Integer facebook;
    private Integer company;
    private String companyId;
    private String companyPw;
}
