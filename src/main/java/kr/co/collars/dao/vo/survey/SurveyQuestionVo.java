package kr.co.collars.dao.vo.survey;

import lombok.Data;

@Data
public class SurveyQuestionVo {
    private Integer id;
    private String question;
    private Integer active;
}
