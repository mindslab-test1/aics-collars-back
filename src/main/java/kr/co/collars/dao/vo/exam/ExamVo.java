package kr.co.collars.dao.vo.exam;

import lombok.Data;

@Data
public class ExamVo {
    private Integer id;
    private String type;
    private String name;
    private Integer data;
    private Float duration;
}
