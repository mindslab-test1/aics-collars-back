package kr.co.collars.dao.vo;

import lombok.Data;

@Data
public class HashTagVo {
    private Integer id;
    private String tag;
}
