package kr.co.collars.dao.vo.exam;

import lombok.Data;

@Data
public class ExamUrlVo {
    private Integer id;
    private String type;
    private String name;
    private String data;
    private Float duration;
}
