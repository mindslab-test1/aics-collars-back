package kr.co.collars.dao.vo.user;

import lombok.Data;

import java.util.Date;

@Data
public class UserSurveyVo {
    private Integer id;
    private Integer userId;
    private Integer questionId;
    private Integer answerId;
    private Date lastUpdate;
}
