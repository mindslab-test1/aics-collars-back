package kr.co.collars.dao.vo.exam;

import lombok.Data;

@Data
public class ExamMongoVo {
    private Integer id;
    private String type;
    private String name;
    private String collection;
    private String objectId;
    private Float duration;
}
