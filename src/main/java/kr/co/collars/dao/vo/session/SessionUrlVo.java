package kr.co.collars.dao.vo.session;

import lombok.Data;

import java.util.Date;

@Data
public class SessionUrlVo {
    private Integer id;
    private String name;
    private Integer duration;
    private String video;
    private String price;
    private String mongoId;
    private String thumbnail;
    private Date createDate;
    private Integer examId;
}
