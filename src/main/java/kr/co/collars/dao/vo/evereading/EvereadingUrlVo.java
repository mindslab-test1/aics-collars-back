package kr.co.collars.dao.vo.evereading;

import lombok.Data;

import java.util.Date;

@Data
public class EvereadingUrlVo {
    private Integer id;
    private String name;
    private String mongoId;
    private String sectorKor;
    private String sectorEng;
    private Integer lev;
    private Date createDate;
    private String thumbnail;
    private String introBanner;
    private String banner;
}
