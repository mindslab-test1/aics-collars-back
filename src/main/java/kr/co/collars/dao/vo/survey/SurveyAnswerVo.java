package kr.co.collars.dao.vo.survey;

import lombok.Data;

@Data
public class SurveyAnswerVo {
    private Integer id;
    private Integer questionId;
    private String answer;
    private String response;
}
