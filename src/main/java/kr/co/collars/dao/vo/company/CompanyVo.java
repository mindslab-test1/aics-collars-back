package kr.co.collars.dao.vo.company;

import lombok.Data;

@Data
public class CompanyVo {
    private Integer id;
    private String name;
    private String code;
}
