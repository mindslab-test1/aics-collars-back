package kr.co.collars.dao.vo.user;

import lombok.Data;

import java.util.Date;

@Data
public class UserExamVo {
    private Integer id;
    private Integer userId;
    private Integer examId;
    private Integer choice;
    private Integer puzzle;
    private Integer speaking;
    private Integer shortAnswer;
    private Integer highlight;
    private Date createDate;
}
