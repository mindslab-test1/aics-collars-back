package kr.co.collars.dao.mapper;

import kr.co.collars.api.auth.data.UserData;
import kr.co.collars.dao.vo.company.CompanyVo;
import kr.co.collars.dao.vo.user.UserVo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper {
    // CREATE
    public int signUpWithProvider(UserVo userVo);
    public int signUpWithCompany(List<UserVo> userVoList);
    // READ
    public UserVo validUser(UserVo userVo);
    public UserData getUser(UserVo userVo);
    public UserVo getCompanyUser(@Param("code") String code, @Param("id") String id);
    public UserVo getLastCompanyUser(CompanyVo companyVo);
    // UPDATE
    public int updateLastSignIn(UserVo userVo);
    public int updateName(UserVo userVo);
    public int updateProvider(@Param("provider") String provider, @Param("email") String email);
    // DELETE
}
