package kr.co.collars.dao.mapper;


import kr.co.collars.api.twocents.model.GetDataListReq;
import kr.co.collars.dao.vo.UpdateMediaVo;
import kr.co.collars.dao.vo.session.SessionUrlVo;
import kr.co.collars.dao.vo.session.SessionVo;
import kr.co.collars.dao.vo.user.UserSessionVo;
import kr.co.collars.dao.vo.user.UserVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SessionMapper {
    // CREATE
    public int add(SessionVo sessionVo);

    // READ
    public SessionVo get(SessionVo sessionVo);

    public SessionUrlVo getData(SessionVo sessionVo);

    public List<SessionUrlVo> getDataList(GetDataListReq getScriptsReq);

    public List<SessionUrlVo> getUserData(UserVo userVo);

    // UPDATE
    public int updateMedia(UpdateMediaVo updateMediaVo);

    public int updateDuration(UserSessionVo userSessionVo);
    // DELETE
}
