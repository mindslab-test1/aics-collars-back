package kr.co.collars.dao.mapper;

import kr.co.collars.api.auth.data.UserSurvey;
import kr.co.collars.api.survey.data.Survey;
import kr.co.collars.dao.vo.survey.SurveyAnswerVo;
import kr.co.collars.dao.vo.survey.SurveyQuestionVo;
import kr.co.collars.dao.vo.user.UserSurveyVo;
import kr.co.collars.dao.vo.user.UserVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SurveyMapper {
    // CREATE
    public int uploadQuestion(SurveyQuestionVo surveyQuestionVo);
    public int uploadAnswer(List<SurveyAnswerVo> answers);
    public int uploadUserSurvey(UserSurveyVo userSurveyVo);
    // READ
    public List<Survey> getSurvey();
    public UserSurveyVo getUserSurvey(UserSurveyVo userSurveyVo);
    public List<UserSurvey> getUserSurveyAll(UserVo userVo);
    // UPDATE
    public int updateUserSurvey(UserSurveyVo userSurveyVo);

    // DELETE
}
