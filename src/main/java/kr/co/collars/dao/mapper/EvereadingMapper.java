package kr.co.collars.dao.mapper;


import kr.co.collars.api.evereading.model.GetDataListReq;
import kr.co.collars.dao.vo.evereading.EvereadingUrlVo;
import kr.co.collars.dao.vo.evereading.EvereadingVo;
import kr.co.collars.dao.vo.HashTagVo;
import kr.co.collars.dao.vo.UpdateMediaVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EvereadingMapper {
    // CREATE
    public int add(EvereadingVo evereadingVo);
    // READ
    public EvereadingVo get(EvereadingVo evereadingVo);
    public EvereadingUrlVo getData(EvereadingVo evereadingVo);
    public List<EvereadingUrlVo> getDataList(GetDataListReq getDataListReq);
    public List<HashTagVo> getSectors(String lang);
    // UPDATE
    public int updateImage(UpdateMediaVo updateImageVo);
    // DELETE
}
