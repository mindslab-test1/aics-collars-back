package kr.co.collars.dao.mapper;

import kr.co.collars.api.purchase.data.UserSession;
import kr.co.collars.dao.vo.user.UserSessionVo;
import org.springframework.stereotype.Repository;

@Repository
public interface PurchaseMapper {
    // CREATE
    public int addSession(UserSession userSession);

    // READ
    public UserSessionVo getSession(UserSession userSession);
    // UPDATE
    // DELETE
}
