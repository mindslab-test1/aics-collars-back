package kr.co.collars.dao.mapper;

import kr.co.collars.dao.vo.HashTagVo;
import org.springframework.stereotype.Repository;

@Repository
public interface HashTagMapper {
    // CREATE
    public int add(HashTagVo hashTagVo);
    // READ
    public HashTagVo get(String tag);
    // UPDATE
    // DELETE
}
