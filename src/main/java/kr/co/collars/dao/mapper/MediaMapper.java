package kr.co.collars.dao.mapper;

import kr.co.collars.dao.vo.MediaVo;
import org.springframework.stereotype.Repository;

@Repository
public interface MediaMapper {
    // CREATE
    public int upload(MediaVo mediaVo);
    // READ
    public MediaVo get(MediaVo mediaVo);
    // UPDATE
    // DELETE
    public int delete(MediaVo mediaVo);
}
