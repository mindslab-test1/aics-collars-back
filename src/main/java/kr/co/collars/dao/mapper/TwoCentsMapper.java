package kr.co.collars.dao.mapper;

import kr.co.collars.api.twocents.model.GetDataListReq;
import kr.co.collars.dao.vo.UpdateMediaVo;
import kr.co.collars.dao.vo.twoCents.TwoCentsUrlVo;
import kr.co.collars.dao.vo.twoCents.TwoCentsVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TwoCentsMapper {
    // CREATE
    public int add(TwoCentsVo twoCentsVo);

    // READ
    public TwoCentsVo get(TwoCentsVo twoCentsVo);
    public TwoCentsUrlVo getData(TwoCentsVo twoCentsVo);
    public List<TwoCentsUrlVo> getDataList(GetDataListReq getScriptsReq);

    // UPDATE
    public int updateImage(UpdateMediaVo updateImageVo);
    // DELETE
}
