package kr.co.collars.dao.mapper;

import kr.co.collars.dao.vo.company.CompanyVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompanyMapper {
    // CREATE
    public int addCompany(CompanyVo companyVo);
    // Read
    public CompanyVo getCompany(CompanyVo companyVo);
    public List<CompanyVo> getCompanies();

}
