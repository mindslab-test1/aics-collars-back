package kr.co.collars.dao.mapper;

import kr.co.collars.dao.vo.mongo.MongoVo;
import org.springframework.stereotype.Repository;

@Repository
public interface MongoMapper {
    // CREATE
    public int add(MongoVo mongoVo);

    // READ
    public MongoVo get(MongoVo mongoVo);
    // UPDATE
    // DELETE
}
