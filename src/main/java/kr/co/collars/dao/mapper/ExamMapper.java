package kr.co.collars.dao.mapper;

import kr.co.collars.api.auth.data.UserExam;
import kr.co.collars.api.exam.model.GetExamListReq;
import kr.co.collars.dao.vo.exam.ExamMongoVo;
import kr.co.collars.dao.vo.exam.ExamUrlVo;
import kr.co.collars.dao.vo.exam.ExamVo;
import kr.co.collars.dao.vo.user.UserExamVo;
import kr.co.collars.dao.vo.user.UserVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExamMapper {
    // CREATE
    public int uploadExam(ExamVo examVo);
    public int uploadResult(UserExamVo userExamVo);
    // READ
    public List<String> getTypes();
    public ExamMongoVo get(ExamVo examVo);
    public List<ExamMongoVo> getList(GetExamListReq getExamListReq);
    public List<UserExam> getUserExams(UserVo userVo);
    // UPDATE
    // DELETE
}
