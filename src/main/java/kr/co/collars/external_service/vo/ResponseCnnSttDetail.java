package kr.co.collars.external_service.vo;

import lombok.Data;

import java.util.List;

@Data
public class ResponseCnnSttDetail {
    private CommonMsg commonMsg;
    private List<CnnSttDetailResult> result;
}
