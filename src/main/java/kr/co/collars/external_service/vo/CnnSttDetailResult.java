package kr.co.collars.external_service.vo;

import lombok.Data;

@Data
public class CnnSttDetailResult {
    private float start;
    private float end;
    private String txt;
}
