package kr.co.collars.external_service.vo;

import lombok.Data;

@Data
public class ResponseCnnStt {
    private CommonMsg commonMsg;
    private String result;
}
