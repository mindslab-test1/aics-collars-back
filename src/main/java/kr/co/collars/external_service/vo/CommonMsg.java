package kr.co.collars.external_service.vo;

import lombok.Data;

@Data
public class CommonMsg {
    private String message;
    private Integer status;
}
