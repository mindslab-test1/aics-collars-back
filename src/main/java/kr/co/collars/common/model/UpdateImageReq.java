package kr.co.collars.common.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Getter
@Setter
public class UpdateImageReq {
    @NotNull
    private Integer id;
    @NotNull
    private String type;
    @NotNull
    private MultipartFile file;
}
