package kr.co.collars.common.data;

import lombok.Data;

@Data
public class Lang {
    private String kor;
    private String eng;
}
