package kr.co.collars.common;

import lombok.Getter;

@Getter
public enum StatusCode {

    // 200
    SUCCESS(2000, "SUCCESS"),
    // 400
    INVALID_PARAMETERS(4001, "INVALID PARAMETERS"),
    // Purchase
    ALREADY_PURCHASED(4900, "This session is already purchased"),
    REQUIRE_PURCHASED(4901, "require purchase"),
    // 500
    // @ Firebase
    UNKNOWN(9999, "UNKNOWN"),
    SIGN_IN_FAIL(5001, "SIGN IN FAIL"),
    FIREBASE_INIT_FAIL(5100, "FIREBASE INIT FAIL"),
    FIREBASE_CREATE_CUSTOM_TOKEN_FAIL(5101, "FIREBASE CREATE CUSTOM TOKEN FAIL"),
    FIREBASE_USER_LOAD_FAIL(5102, "FIREBASE USER LOAD FAIL"),
    // @ Database
    DB_FAIL(5200, "DB FAIL"),
    DB_CREATE_USER_FAIL(5201, "DB CREATE USER FAIL"),
    DB_VALID_FAIL(5202, "DB VALID FAIL"),
    DB_UPDATE_USER_FAIL(5203, "DB UPDATE USER FAIL"),
    DB_UPLOAD_FAIL(5204, "Upload data into database is failed"),
    DB_LOAD_FAIL(5205, "Load data from database is failed"),
    // @ MongoDB
    MONGO_UPLOAD_FAIL(5210, "Upload data into MongoDB is failed"),
    // @DATA
    DATA_UPDATE_FAIL(5206, "Update data failed"),
    // @ Media Server
    MEDIA_SERVER_UPLOAD_FAIL(5300, "MEDIA SERVER UPLOAD FAIL"),
    MEDIA_SERVER_DELETE_FAIL(5301, "MEDIA SERVER DELETE FAIL"),
    // @ Maum Server
    MAUM_SERVER_FAIL(5400, "Can not get result from maum.ai api server"),
    // @ UPLOAD
    UPLOAD_FAIL(5500, "UPLOAD FAIL"),
    UPLOAD_IMAGE_FAIL(5501, "UPLOAD IMAGE FAIL");


    private final int code;
    private final String message;

    StatusCode(int code, String message) {
        this.code = code;
        this.message = message;
    }
}
