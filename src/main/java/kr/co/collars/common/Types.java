package kr.co.collars.common;

public class Types {
    private Types(){}

    // Sign In Types
    public static final String PASSWORD = "PASSWORD";
    public static final String GOOGLE = "GOOGLE";
    public static final String NAVER = "NAVER";
    public static final String KAKAO = "KAKAO";
    public static final String FACEBOOK = "FACEBOOK";

    // Media
    // - Video
    public static final String VIDEO = "VIDEO";
    // - Images
    public static final String THUMBNAIL = "THUMBNAIL";
    public static final String INTRO_BANNER = "INTRO_BANNER";
    public static final String BANNER = "BANNER";

    // Evereading Image Types
    public static final String EVEREADING_THUMBNAIL = "thumbnail";
    public static final String EVEREADING_INTRO_BANNER = "intro_banner";
    public static final String EVEREADING_BANNER = "banner";

    // Exam Typs
    public static final String LEVEL = "LEVEL";
    public static final String SESSION = "SESSION";

    // Exam Result
    public static final String CHOICE = "CHOICE";
    public static final String PUZZLE = "PUZZLE";
    public static final String SPEAKING = "SPEAKING";
    public static final String SHORT_ANSWER = "SHORT_ANSWER";
    public static final String HIGHLIGHT = "HIGHLIGHT";

}
