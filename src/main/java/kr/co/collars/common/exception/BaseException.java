package kr.co.collars.common.exception;

import org.springframework.http.HttpStatus;

public class BaseException extends RuntimeException {

    private final Integer code;
    private final String message;
    private final Object payload;

    public BaseException(Integer code, String message, Object payload) {
        super(message);
        this.code = code;
        this.message = message;
        this.payload = payload;
    }

}
