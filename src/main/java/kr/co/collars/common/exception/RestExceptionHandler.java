package kr.co.collars.common.exception;

import kr.co.collars.common.CommonRes;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CustomInternalException.class)
    public ResponseEntity<Object> handleCustomInternalException(CustomInternalException exception, WebRequest request) {
        CommonRes commonRes = new CommonRes(exception.getCode(), exception.getMessage(), exception.getPayload());
        return super.handleExceptionInternal(exception, commonRes, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @ExceptionHandler(CustomBadRequest.class)
    public ResponseEntity<Object> handleCustomBadRequest(CustomBadRequest exception, WebRequest request) {
        CommonRes commonRes = new CommonRes(exception.getCode(), exception.getMessage(), exception.getPayload());
        return super.handleExceptionInternal(exception, commonRes, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }
}
