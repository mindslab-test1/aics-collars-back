package kr.co.collars.common.exception;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
@Getter
@Setter
public class CustomInternalException extends BaseException {
    private final Integer code;
    private final String message;
    private final Object payload;

    public CustomInternalException(Integer code, String message, Object payload) {
        super(code, message, payload);
        this.code = code;
        this.message = message;
        this.payload = payload;
    }
}
