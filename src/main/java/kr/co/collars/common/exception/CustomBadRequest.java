package kr.co.collars.common.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
@Getter
@Setter
public class CustomBadRequest extends BaseException {
    private final Integer code;
    private final String message;
    private final Object payload;

    public CustomBadRequest(Integer code, String message, Object payload) {
        super(code, message, payload);
        this.code = code;
        this.message = message;
        this.payload = payload;
    }
}
