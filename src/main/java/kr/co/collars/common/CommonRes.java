package kr.co.collars.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class CommonRes {
    private Integer code;
    private String message;
    private Object payload;
}
