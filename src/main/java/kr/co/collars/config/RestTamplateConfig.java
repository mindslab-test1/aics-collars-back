package kr.co.collars.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

@Slf4j
@Configuration
public class RestTamplateConfig {
    @Bean
    public RestTemplate restTemplate() {
        int DEFAULT_TIMEOUT = 10 * 60 * 1000; // 10 Minutes

        RestTemplate restTemplate = new RestTemplateBuilder()
                .setConnectTimeout(Duration.ofMillis(DEFAULT_TIMEOUT))
                .build();

        return restTemplate;
    }
}
