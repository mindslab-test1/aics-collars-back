package kr.co.collars.utils.exception;

import kr.co.collars.common.StatusCode;
import kr.co.collars.common.exception.CustomBadRequest;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;

@Component
public class ValidRequest {
    public void check(BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            throw new CustomBadRequest(StatusCode.INVALID_PARAMETERS.getCode(), StatusCode.INVALID_PARAMETERS.getMessage(), bindingResult);
        }
    }
}
