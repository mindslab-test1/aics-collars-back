package kr.co.collars.utils.helper;

import kr.co.collars.common.StatusCode;
import kr.co.collars.common.exception.CustomInternalException;
import kr.co.collars.dao.mapper.HashTagMapper;
import kr.co.collars.dao.vo.HashTagVo;
import lombok.RequiredArgsConstructor;
import oracle.security.pki.util.EccCurveParameters;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Transactional
@Component
public class HashTagHelper {
    final HashTagMapper hashTagMapper;

    public HashTagVo get(String tag){
        try {
            HashTagVo hashTagVo = hashTagMapper.get(tag);
            if (hashTagVo == null) {
                HashTagVo newHashTagVo = new HashTagVo();
                newHashTagVo.setTag(tag);
                hashTagMapper.add(newHashTagVo);
                hashTagVo = newHashTagVo;
            }
            return hashTagVo;
        }catch (Exception e){
            throw new CustomInternalException(StatusCode.DB_FAIL.getCode(),StatusCode.DB_FAIL.getMessage(), e.getMessage());
        }
    }
}
