package kr.co.collars.utils.helper;

import kr.co.collars.dao.mapper.MongoMapper;
import kr.co.collars.dao.vo.mongo.MongoVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Transactional
@Component
public class MongoHelper {
    final MongoMapper mongoMapper;

    public MongoVo add(String collection, String objectId){
        MongoVo mongoVo =new MongoVo();
        mongoVo.setCollection(collection);
        mongoVo.setObjectId(objectId);
        mongoMapper.add(mongoVo);
        return mongoVo;
    }

    public String getObjectId(Integer id){
        MongoVo mongoVo =new MongoVo();
        mongoVo.setId(id);
        MongoVo result = mongoMapper.get(mongoVo);
        return result.getObjectId();
    }

}
