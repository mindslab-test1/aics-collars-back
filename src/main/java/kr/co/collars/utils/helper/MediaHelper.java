package kr.co.collars.utils.helper;

import kr.co.collars.dao.mapper.MediaMapper;
import kr.co.collars.dao.vo.MediaVo;
import kr.co.collars.utils.media_server.MediaServer;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Transactional
@Component
public class MediaHelper {
    final MediaServer mediaServer;
    final MediaMapper mediaMapper;

    public MediaVo upload(String contentType, String url) {
        MediaVo mediaVo = new MediaVo();
        mediaVo.setContentType(contentType);
        mediaVo.setUrl(url);
        mediaMapper.upload(mediaVo);
        return mediaVo;
    }

    public void delete(Integer id){
        MediaVo mediaVo = new MediaVo();
        mediaVo.setId(id);
        MediaVo result = mediaMapper.get(mediaVo);
        mediaServer.delete(result.getUrl());
        mediaMapper.delete(result);
    }
}
