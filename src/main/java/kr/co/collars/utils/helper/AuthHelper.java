package kr.co.collars.utils.helper;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import kr.co.collars.common.StatusCode;
import kr.co.collars.common.exception.CustomInternalException;
import kr.co.collars.dao.mapper.UserMapper;
import kr.co.collars.dao.vo.user.UserVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Transactional
@Component
public class AuthHelper {
    final UserMapper userMapper;

    public int getId(Integer id, String uid) {
        try {
            int result;
            if(uid != null) {
                UserRecord userRecord = FirebaseAuth.getInstance().getUser(uid);
                UserVo userVo = new UserVo();
                userVo.setEmail(userRecord.getEmail());
                UserVo data = userMapper.validUser(userVo);

                result = data.getId();
            }
            else {
                result = id;
            }

            return result;
        } catch (FirebaseAuthException e) {
            throw new CustomInternalException(StatusCode.FIREBASE_USER_LOAD_FAIL.getCode(), StatusCode.FIREBASE_USER_LOAD_FAIL.getMessage(), e.getMessage());
        }
    }
}
