package kr.co.collars.utils.media_server;

import kr.co.collars.api.exam.data.json.ExamData;
import kr.co.collars.common.StatusCode;
import kr.co.collars.common.exception.CustomInternalException;
import kr.co.collars.external_service.vo.ResponseUploadedFileInfo;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@RequiredArgsConstructor
@Component
public class MediaServer {
    private final RestTemplate restTemplate;

    @Value("${media.server.url}")
    private String MEDIA_SERVER_URL;
    @Value("${media.server.port}")
    private String MEDIA_SERVER_PORT;
    @Value("${media.server.dir}")
    private String MEDIA_SERVER_DIR;

    public Object get(String url, Class type){
        URI getUri = URI.create(MEDIA_SERVER_URL + url);

        return restTemplate.getForObject(getUri, type);
    }

    public String upload(MultipartFile file) {
        URI uploadUri = URI.create(MEDIA_SERVER_URL + "/media/file:upload");

        HttpHeaders requestMasterHeaders = new HttpHeaders();
        requestMasterHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", file.getResource());
        body.add("uploadDirectory", MEDIA_SERVER_DIR);
        HttpEntity<MultiValueMap<String, Object>> requestMasterHttpEntity = new HttpEntity<>(body, requestMasterHeaders);
        ResponseEntity<ResponseUploadedFileInfo> response = restTemplate.exchange(uploadUri, HttpMethod.POST, requestMasterHttpEntity, new ParameterizedTypeReference<ResponseUploadedFileInfo>() {
        });
        if(response.getStatusCode().value() != 201){
            throw new CustomInternalException(StatusCode.MEDIA_SERVER_UPLOAD_FAIL.getCode(), StatusCode.MEDIA_SERVER_UPLOAD_FAIL.getMessage(), response.getBody());
        }
        return extractMediaPath(response.getBody().getFileDownloadUri());
    }
    public String upload(ByteArrayResource file) {
        URI uploadUri = URI.create(MEDIA_SERVER_URL + "/media/file:upload");

        HttpHeaders requestMasterHeaders = new HttpHeaders();
        requestMasterHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("file", file);
        body.add("uploadDirectory", MEDIA_SERVER_DIR);
        HttpEntity<MultiValueMap<String, Object>> requestMasterHttpEntity = new HttpEntity<>(body, requestMasterHeaders);
        ResponseEntity<ResponseUploadedFileInfo> response = restTemplate.exchange(uploadUri, HttpMethod.POST, requestMasterHttpEntity, new ParameterizedTypeReference<ResponseUploadedFileInfo>() {
        });
        if(response.getStatusCode().value() != 201){
            throw new CustomInternalException(StatusCode.MEDIA_SERVER_UPLOAD_FAIL.getCode(), StatusCode.MEDIA_SERVER_UPLOAD_FAIL.getMessage(), response.getBody());
        }
        return extractMediaPath(response.getBody().getFileDownloadUri());
    }

    public void delete(String url) {
        URI deleteUri = URI.create(MEDIA_SERVER_URL + "/media/file:delete");
        String formattedUrl = url.split("/media")[1];

        try {
            HttpHeaders requestMasterHeaders = new HttpHeaders();
            requestMasterHeaders.setContentType(MediaType.APPLICATION_JSON);
            Map<String, String> body = new HashMap<>();
            body.put("deleteFileFullPath", formattedUrl);
            HttpEntity<Map<String, String>> requestMasterHttpEntity = new HttpEntity<>(body, requestMasterHeaders);
            restTemplate.exchange(deleteUri, HttpMethod.DELETE, requestMasterHttpEntity, Void.class);
        }catch (Exception e){
            throw new CustomInternalException(StatusCode.MEDIA_SERVER_DELETE_FAIL.getCode(), StatusCode.MEDIA_SERVER_DELETE_FAIL.getMessage(), e.getMessage());
        }
    }

    // Function
    public String extractMediaPath(String url) {
        String result[] = url.split("(?=/media/aics)");
        return result[1];
    }
}
