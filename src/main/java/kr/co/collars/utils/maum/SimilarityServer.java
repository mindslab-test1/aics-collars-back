package kr.co.collars.utils.maum;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.RequiredArgsConstructor;
import maum.brain.similarity.SimilarityGrpc;
import maum.brain.similarity.SimilarityGrpc.*;
import maum.brain.similarity.SimilarityOuterClass;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;

//@RequiredArgsConstructor
@Component
public class SimilarityServer {
    private SimilarityBlockingStub similarityBlockingStub;

    @Value("${maum.ai.similarity.server}")
    private String SIMILARITY_SERVER;
    private ManagedChannel channel;

    public void setDestination() {
        channel = ManagedChannelBuilder.forTarget(SIMILARITY_SERVER).usePlaintext().build();
        similarityBlockingStub = SimilarityGrpc.newBlockingStub(channel);
    }

    public double GetScore(String userAnswer, List<String> answers) throws InterruptedException {
        try {
            SimilarityOuterClass.Input input = SimilarityOuterClass.Input.newBuilder().setUserAnswer(userAnswer).addAllAnswers(answers).build();
            SimilarityOuterClass.Output output = similarityBlockingStub.getScore(input);
            return output.getData();
        } finally {
            channel.shutdownNow().awaitTermination(5, TimeUnit.SECONDS);
        }
    }
}
