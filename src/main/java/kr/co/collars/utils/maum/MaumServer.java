package kr.co.collars.utils.maum;


import kr.co.collars.external_service.vo.CnnSttDetailResult;
import kr.co.collars.external_service.vo.ResponseCnnStt;
import kr.co.collars.external_service.vo.ResponseCnnSttDetail;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.net.URI;
import java.util.List;

@RequiredArgsConstructor
@Component
public class MaumServer {
    private final RestTemplate restTemplate;

    @Value("${maum.ai.server}")
    private String MAUM_SERVER;
    @Value("${maum.ai.api.id}")
    private String MAUM_API_ID;
    @Value("${maum.ai.api.key}")
    private String MAUM_API_KEY;
    @Value("${maum.ai.api.cnnSttDetail.lang}")
    private String LANG;
    @Value("${maum.ai.api.cnnSttDetail.model}")
    private String MODEL;
    @Value("${maum.ai.api.cnnSttDetail.samplerate}")
    private String SAMPLERATE;
    @Value("${maum.ai.api.tts.voiceName}")
    private String VOICENAME;

    public String cnnStt(MultipartFile file) {
        URI uri = URI.create(MAUM_SERVER + "/stt/cnnStt");
        HttpHeaders requestMasterHeaders = new HttpHeaders();
        requestMasterHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("apiId", MAUM_API_ID);
        body.add("apiKey", MAUM_API_KEY);
        body.add("file", file.getResource());
        HttpEntity<MultiValueMap<String, Object>> requestMasterHttpEntity = new HttpEntity<>(body, requestMasterHeaders);
        ResponseEntity<ResponseCnnStt> response = restTemplate.exchange(uri, HttpMethod.POST, requestMasterHttpEntity, ResponseCnnStt.class);
        return response.getBody().getResult();
    }

    public String cnnSttDetail(MultipartFile file) {
        URI uri = URI.create(MAUM_SERVER + "/stt/cnnSttDetail");
        HttpHeaders requestMasterHeaders = new HttpHeaders();
        requestMasterHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("apiId", MAUM_API_ID);
        body.add("apiKey", MAUM_API_KEY);
        body.add("file", file.getResource());
        body.add("lang", "kor");
        body.add("model", "minutes_cnn_8k_kr");
        body.add("samplerate", 8000);
        HttpEntity<MultiValueMap<String, Object>> requestMasterHttpEntity = new HttpEntity<>(body, requestMasterHeaders);
        ResponseEntity<ResponseCnnSttDetail> response = restTemplate.exchange(uri, HttpMethod.POST, requestMasterHttpEntity, ResponseCnnSttDetail.class);

        String result = "";
        for(CnnSttDetailResult e : response.getBody().getResult()){
            String txt = e.getTxt();
            if(txt.contains("\n")){
                String replacedTxt = txt.trim().replaceAll("\\n", "");
                result += replacedTxt + " ";
            }
            else{
                result += txt + " ";
            }
        }
        return result;
    }

    public byte[] tts(String text){
        URI uri = URI.create(MAUM_SERVER + "/tts/stream");
        HttpHeaders requestMasterHeaders = new HttpHeaders();
        requestMasterHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
        body.add("apiId", MAUM_API_ID);
        body.add("apiKey", MAUM_API_KEY);
        body.add("voiceName", VOICENAME);
        body.add("text", text);
        HttpEntity<MultiValueMap<String, Object>> requestMasterHttpEntity = new HttpEntity<>(body, requestMasterHeaders);
        ResponseEntity<byte[]> response = restTemplate.exchange(uri, HttpMethod.POST, requestMasterHttpEntity, byte[].class);
        return response.getBody();
    }
}
