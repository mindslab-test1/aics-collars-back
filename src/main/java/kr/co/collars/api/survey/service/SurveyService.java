package kr.co.collars.api.survey.service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import kr.co.collars.api.auth.service.AuthService;
import kr.co.collars.api.survey.data.Answer;
import kr.co.collars.api.survey.data.Survey;
import kr.co.collars.api.survey.data.SurveyData;
import kr.co.collars.api.survey.data.SurveyResult;
import kr.co.collars.api.survey.model.UploadSurveyReq;
import kr.co.collars.api.survey.model.UploadSurveyResultReq;
import kr.co.collars.common.CommonRes;
import kr.co.collars.common.StatusCode;
import kr.co.collars.common.exception.CustomInternalException;
import kr.co.collars.dao.mapper.SurveyMapper;
import kr.co.collars.dao.vo.survey.SurveyAnswerVo;
import kr.co.collars.dao.vo.survey.SurveyQuestionVo;
import kr.co.collars.dao.vo.user.UserSurveyVo;
import kr.co.collars.utils.helper.AuthHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Slf4j
@Transactional
@Service
public class SurveyService {
    private final AuthHelper authHelper;
    private final AuthService authService;
    final SurveyMapper surveyMapper;

    public CommonRes getSurvey() {
        try {
            List<Survey> surveys = surveyMapper.getSurvey();
            List<SurveyData> surveyDataList = new ArrayList<>();
            for (Survey survey : surveys) {
                SurveyData surveyData = new SurveyData();
                surveyData.setId(survey.getId());
                surveyData.setQuestion(survey.getQuestion());
                List<Answer> answers = new Gson().fromJson(survey.getAnswers(), new TypeToken<List<Answer>>() {
                }.getType());
                surveyData.setAnswers(answers);
                surveyDataList.add(surveyData);
            }

            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), surveyDataList);
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.DB_LOAD_FAIL.getCode(), StatusCode.DB_LOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    public CommonRes uploadSurvey(UploadSurveyReq req) {
        try {
            // Upload survey question
            SurveyQuestionVo surveyQuestionVo = new SurveyQuestionVo();
            surveyQuestionVo.setQuestion(req.getQuestion());
            surveyMapper.uploadQuestion(surveyQuestionVo);
            // Upload survey answer
            List<SurveyAnswerVo> answers = new ArrayList<>();
            for (Answer answer : req.getAnswers()) {
                SurveyAnswerVo surveyAnswerVo = new SurveyAnswerVo();
                surveyAnswerVo.setQuestionId(surveyQuestionVo.getId());
                surveyAnswerVo.setAnswer(answer.getAnswer());
                surveyAnswerVo.setResponse(answer.getResponse());
                answers.add(surveyAnswerVo);
            }
            surveyMapper.uploadAnswer(answers);

            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), true);
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.DB_UPLOAD_FAIL.getCode(), StatusCode.DB_UPLOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    public CommonRes uploadSurveyResult(UploadSurveyResultReq req) {
        try {
            int id = authHelper.getId(req.getId(), req.getUid());
            for (SurveyResult surveyResult : req.getResult()) {
                UserSurveyVo userSurveyVo = new UserSurveyVo();
                userSurveyVo.setUserId(id);
                userSurveyVo.setQuestionId(surveyResult.getQuestion());
                userSurveyVo.setAnswerId(surveyResult.getAnswer());

                UserSurveyVo oldUserSurveyVo = surveyMapper.getUserSurvey(userSurveyVo);
                if (oldUserSurveyVo == null) {
                    surveyMapper.uploadUserSurvey(userSurveyVo);
                } else {
                    surveyMapper.updateUserSurvey(userSurveyVo);
                }
            }
            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), true);
        } catch (Exception e) {
            log.debug(e.getMessage());
            throw new CustomInternalException(StatusCode.DB_UPLOAD_FAIL.getCode(), StatusCode.DB_UPLOAD_FAIL.getMessage(), e.getMessage());
        }
    }
}
