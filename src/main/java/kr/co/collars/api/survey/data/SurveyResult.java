package kr.co.collars.api.survey.data;

import lombok.Data;

@Data
public class SurveyResult {
    private Integer question;
    private Integer answer;
}
