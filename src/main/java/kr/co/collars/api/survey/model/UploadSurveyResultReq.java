package kr.co.collars.api.survey.model;

import kr.co.collars.api.survey.data.SurveyResult;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class UploadSurveyResultReq {
    private Integer id;
    private String uid;
    @NotNull
    private List<SurveyResult> result;
}
