package kr.co.collars.api.survey.controller;

import kr.co.collars.api.survey.model.UploadSurveyReq;
import kr.co.collars.api.survey.model.UploadSurveyResultReq;
import kr.co.collars.api.survey.service.SurveyService;
import kr.co.collars.common.CommonRes;
import kr.co.collars.utils.exception.ValidRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/survey")
public class SurveyController {
    private final SurveyService surveyService;
    private final ValidRequest validRequest;

    @GetMapping
    public CommonRes getSurvey() {
        return surveyService.getSurvey();
    }


    @PostMapping("/data:upload")
    public CommonRes uploadSurvey(
            @Valid @RequestBody UploadSurveyReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return surveyService.uploadSurvey(req);
    }

    @PostMapping("result:upload")
    public CommonRes uploadSurveyResult(
            @Valid @RequestBody UploadSurveyResultReq req,
            BindingResult bindingResult
    ){
        validRequest.check(bindingResult);
        return surveyService.uploadSurveyResult(req);
    }

}
