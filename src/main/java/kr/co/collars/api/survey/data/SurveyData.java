package kr.co.collars.api.survey.data;

import lombok.Data;

import javax.swing.*;
import java.util.List;

@Data
public class SurveyData {
    private Integer id;
    private String question;
    private List<Answer> answers;
}
