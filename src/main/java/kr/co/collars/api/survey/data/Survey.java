package kr.co.collars.api.survey.data;

import lombok.Data;

@Data
public class Survey {
    public Integer id;
    private String question;
    private String answers;
}
