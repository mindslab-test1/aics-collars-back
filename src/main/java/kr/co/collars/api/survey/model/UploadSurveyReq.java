package kr.co.collars.api.survey.model;


import kr.co.collars.api.survey.data.Answer;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class UploadSurveyReq {
    @NotBlank
    private String question;
    private List<Answer> answers;
}
