package kr.co.collars.api.survey.data;

import lombok.Data;

@Data
public class Answer {
    private Integer id;
    private String answer;
    private String response;
}
