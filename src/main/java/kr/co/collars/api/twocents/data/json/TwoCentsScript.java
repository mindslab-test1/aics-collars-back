package kr.co.collars.api.twocents.data.json;

import lombok.Data;

import java.util.List;

@Data
public class TwoCentsScript {
    private Title title;
    private List<Content> content;
    private List<Vocabulary> vocabulary;
}
