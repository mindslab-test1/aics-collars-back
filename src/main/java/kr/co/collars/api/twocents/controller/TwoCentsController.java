package kr.co.collars.api.twocents.controller;

import kr.co.collars.api.twocents.model.GetDataListReq;
import kr.co.collars.api.twocents.model.UploadAudioReq;
import kr.co.collars.api.twocents.model.UploadDataReq;
import kr.co.collars.api.twocents.service.TwoCentsService;
import kr.co.collars.common.CommonRes;
import kr.co.collars.common.StatusCode;
import kr.co.collars.common.exception.CustomInternalException;
import kr.co.collars.common.model.GetById;
import kr.co.collars.common.model.UpdateImageReq;
import kr.co.collars.utils.exception.ValidRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static kr.co.collars.common.Types.*;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/twoCents")
public class TwoCentsController {
    private final TwoCentsService twoCentsService;
    private final ValidRequest validRequest;

    // CREATE
    @PostMapping("/data:upload")
    public CommonRes uploadData(
            @Valid @RequestBody UploadDataReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return twoCentsService.uploadData(req);
    }

    @PostMapping(path = "/audio:upload", produces = "application/json")
    public CommonRes uploadAudio(
            @Valid UploadAudioReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return twoCentsService.uploadAudio(req);
    }

    // READ
    @PostMapping("/data")
    public CommonRes getData(
            @Valid @RequestBody GetById req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return twoCentsService.getData(req);
    }

    @PostMapping("/data:list")
    public CommonRes getDataList(
            @Valid @RequestBody GetDataListReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return twoCentsService.getDataList(req);
    }

    // UPDATE
    @PostMapping("/image:update")
    public CommonRes updateImage(
            @Valid UpdateImageReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        String type = req.getType();
        if (type.equalsIgnoreCase(THUMBNAIL) || type.equalsIgnoreCase(INTRO_BANNER) || type.equalsIgnoreCase(BANNER)) {
            return twoCentsService.updateImage(req);
        } else {
            throw new CustomInternalException(StatusCode.INVALID_PARAMETERS.getCode(), StatusCode.INVALID_PARAMETERS.getMessage(), "Type should one of [" + THUMBNAIL + ", " + INTRO_BANNER + ", " + BANNER + "]");
        }
    }
}
