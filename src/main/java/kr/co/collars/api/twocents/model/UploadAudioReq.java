package kr.co.collars.api.twocents.model;

import lombok.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;


@NoArgsConstructor
@Getter
@Setter
public class UploadAudioReq {
    @NonNull
    private Integer id;
    @NonNull
    private List<Integer> index;
    @NonNull
    private List<MultipartFile> file;
}
