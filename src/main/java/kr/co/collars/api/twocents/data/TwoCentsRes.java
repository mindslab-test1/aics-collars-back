package kr.co.collars.api.twocents.data;

import kr.co.collars.api.twocents.data.json.TwoCentsScript;
import kr.co.collars.common.data.Lang;
import lombok.Data;

import java.util.Date;

@Data
public class TwoCentsRes {
    private Integer id;
    private String name;
    private Lang sector;
    private Integer lev;
    private Date createDate;
    private TwoCentsScript data;
    private TwoCentsLinks links;
}
