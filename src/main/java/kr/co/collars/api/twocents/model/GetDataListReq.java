package kr.co.collars.api.twocents.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Getter
@Setter
public class GetDataListReq {
    @NotNull
    private Integer current;
    @NotNull
    private Integer increment;
    private Boolean data = false;
}
