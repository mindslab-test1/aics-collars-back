package kr.co.collars.api.twocents.model;

import kr.co.collars.api.twocents.data.json.TwoCentsScript;
import kr.co.collars.common.data.Lang;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Getter
@Setter
public class UploadDataReq {
    @NotNull
    private String name;
    private Lang sector;
    @NotNull
    private Integer level;
    private TwoCentsScript data;
}
