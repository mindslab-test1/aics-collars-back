package kr.co.collars.api.twocents.data.json;

import lombok.Data;

@Data
public class Vocabulary {
    private Integer index;
    private String eng;
    private String kor;
}
