package kr.co.collars.api.twocents.data;

import lombok.Data;

@Data
public class TwoCentsLinks {
    private String thumbnail;
    private String introBanner;
    private String banner;
}
