package kr.co.collars.api.twocents.data.json;

import lombok.Data;

@Data
public class Content {
    private String floating;
    private String name;
    private String eng;
    private String kor;
    private String audio;
}
