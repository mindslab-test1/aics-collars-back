package kr.co.collars.api.twocents.data.json;

import lombok.Data;

@Data
public class Title {
    private String eng;
    private String kor;
}
