package kr.co.collars.api.twocents.service;

import kr.co.collars.api.twocents.data.TwoCentsLinks;
import kr.co.collars.api.twocents.data.TwoCentsRes;
import kr.co.collars.api.twocents.data.json.Content;
import kr.co.collars.api.twocents.model.GetDataListReq;
import kr.co.collars.api.twocents.model.UploadAudioReq;
import kr.co.collars.api.twocents.model.UploadDataReq;
import kr.co.collars.common.CommonRes;
import kr.co.collars.common.StatusCode;
import kr.co.collars.common.data.Lang;
import kr.co.collars.common.exception.CustomInternalException;
import kr.co.collars.common.model.GetById;
import kr.co.collars.common.model.UpdateImageReq;
import kr.co.collars.dao.mapper.MongoMapper;
import kr.co.collars.dao.mapper.TwoCentsMapper;
import kr.co.collars.dao.vo.HashTagVo;
import kr.co.collars.dao.vo.MediaVo;
import kr.co.collars.dao.vo.UpdateMediaVo;
import kr.co.collars.dao.vo.mongo.MongoVo;
import kr.co.collars.dao.vo.twoCents.TwoCentsUrlVo;
import kr.co.collars.dao.vo.twoCents.TwoCentsVo;
import kr.co.collars.mongo.entity.TwoCentsEntity;
import kr.co.collars.utils.helper.HashTagHelper;
import kr.co.collars.utils.helper.MediaHelper;
import kr.co.collars.utils.media_server.MediaServer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static kr.co.collars.common.Types.*;

@RequiredArgsConstructor
@Slf4j
@Transactional
@Service
public class TwoCentsService {
    private final MediaServer mediaServer;
    private final MediaHelper mediaHelper;
    private final HashTagHelper hashTagHelper;

    final TwoCentsMapper twoCentsMapper;

    final MongoMapper mongoMapper;
    final MongoTemplate mongoTemplate;

    @Value("${media.server.url}")
    private String MEDIA_SERVER_URL;


    // CREAET
    public CommonRes uploadData(UploadDataReq req) {
        try {
            // Mongo
            TwoCentsEntity twoCentsEntity = new TwoCentsEntity();
            twoCentsEntity.setData(req.getData());
            mongoTemplate.save(twoCentsEntity);
            // Oracle(CLS_MONGO)
            MongoVo mongoVo = new MongoVo();
            mongoVo.setCollection(mongoTemplate.getCollectionName(TwoCentsEntity.class));
            mongoVo.setObjectId(twoCentsEntity.get_id());
            mongoMapper.add(mongoVo);
            // Oracle(CLS_TWOCENTS)
            HashTagVo sectorKor = hashTagHelper.get(req.getSector().getKor());
            HashTagVo sectorEng = hashTagHelper.get(req.getSector().getEng());
            TwoCentsVo twoCentsVo = new TwoCentsVo();
            twoCentsVo.setName(req.getName());
            twoCentsVo.setSectorEng(sectorEng.getId());
            twoCentsVo.setSectorKor(sectorKor.getId());
            twoCentsVo.setLev(req.getLevel());
            twoCentsVo.setMongoId(mongoVo.getId());

            twoCentsMapper.add(twoCentsVo);

            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), twoCentsVo);
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.DB_UPLOAD_FAIL.getCode(), StatusCode.DB_UPLOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    public CommonRes uploadAudio(UploadAudioReq req) {
        List<Integer> audioIndex = req.getIndex();
        List<String> audioLinks = new ArrayList<>();
        List<String> oldAudioLinks = new ArrayList<>();
        try {
            TwoCentsVo twoCentsVo = new TwoCentsVo();
            twoCentsVo.setId(req.getId());
            TwoCentsUrlVo result = twoCentsMapper.getData(twoCentsVo);

            Query query = Query.query(Criteria.where("_id").is(result.getMongoId()));
            TwoCentsEntity entity = mongoTemplate.findOne(query, TwoCentsEntity.class);

            List<Content> contents = entity.getData().getContent();

            for (int i = 0; i < contents.size(); i++) {
                if (audioIndex.contains(i)) {
                    int fileIndex = audioIndex.indexOf(i);
                    Content content = contents.get(i);

                    if (!content.getAudio().equals("") && !content.getAudio().equals(null)) {
                        oldAudioLinks.add(content.getAudio());
                    }
                    String mediaUrl = mediaServer.upload(req.getFile().get(fileIndex));
                    audioLinks.add(mediaUrl);
                    content.setAudio(mediaUrl);
                }
            }

            mongoTemplate.save(entity);
            for (String oldAudioLink : oldAudioLinks) {
                mediaServer.delete(oldAudioLink);
            }

            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), entity);
        } catch (Exception e) {
            if (audioLinks.size() > 0) {
                for (String audioLink : audioLinks) {
                    mediaServer.delete(audioLink);
                }
            }
            throw new CustomInternalException(StatusCode.DATA_UPDATE_FAIL.getCode(), StatusCode.DATA_UPDATE_FAIL.getMessage(), e.getMessage());
        }
    }

    // READ

    private TwoCentsRes formattingData(TwoCentsUrlVo data, Boolean detail) {
        TwoCentsRes response = new TwoCentsRes();
        response.setId(data.getId());
        response.setName(data.getName());
        response.setLev(data.getLev());
        response.setCreateDate(data.getCreateDate());

        Lang sector = new Lang();
        sector.setEng(data.getSectorEng());
        sector.setKor(data.getSectorKor());
        response.setSector(sector);

        TwoCentsLinks twoCentsLinks = new TwoCentsLinks();
        if (data.getThumbnail() != null)
            twoCentsLinks.setThumbnail(MEDIA_SERVER_URL + data.getThumbnail());
        if (data.getIntroBanner() != null)
            twoCentsLinks.setIntroBanner(MEDIA_SERVER_URL + data.getIntroBanner());
        if (data.getBanner() != null)
            twoCentsLinks.setBanner(MEDIA_SERVER_URL + data.getBanner());
        response.setLinks(twoCentsLinks);

        if (data.getMongoId() != null && detail) {
            Query query = Query.query(Criteria.where("_id").is(data.getMongoId()));
            TwoCentsEntity entity = mongoTemplate.findOne(query, TwoCentsEntity.class);
            for (Content content : entity.getData().getContent()) {
                if (!content.getAudio().equals("")) {
                    content.setAudio(MEDIA_SERVER_URL + content.getAudio());
                }
            }
            response.setData(entity.getData());
        }

        return response;
    }

    public CommonRes getData(GetById req) {
        try {
            TwoCentsVo twoCentsVo = new TwoCentsVo();
            twoCentsVo.setId(req.getId());
            TwoCentsUrlVo twoCentsUrlVo = twoCentsMapper.getData(twoCentsVo);

            TwoCentsRes response = formattingData(twoCentsUrlVo, true);

            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), response);
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.DB_LOAD_FAIL.getCode(), StatusCode.DB_LOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    public CommonRes getDataList(GetDataListReq req) {
        try {
            List<TwoCentsUrlVo> twoCentsUrlVoList = twoCentsMapper.getDataList(req);
            List<TwoCentsRes> responseList = new ArrayList<>();
            for (int i = 0; i < twoCentsUrlVoList.size(); i++) {
                TwoCentsRes response = formattingData(twoCentsUrlVoList.get(i), req.getData());
                responseList.add(response);
            }
            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), responseList);
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.DB_LOAD_FAIL.getCode(), StatusCode.DB_LOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    // UPDATE
    public CommonRes updateImage(UpdateImageReq req) {
        String mediaUrl = mediaServer.upload(req.getFile());
        try {
            MediaVo mediaVo = mediaHelper.upload(req.getFile().getContentType(), mediaUrl);

            TwoCentsVo twoCentsVo = new TwoCentsVo();
            twoCentsVo.setId(req.getId());
            TwoCentsVo result = twoCentsMapper.get(twoCentsVo);

            UpdateMediaVo updateMediaVo = new UpdateMediaVo();
            updateMediaVo.setId(result.getId());
            updateMediaVo.setMediaId(mediaVo.getId());
            updateMediaVo.setType(req.getType());
            twoCentsMapper.updateImage(updateMediaVo);

            switch (req.getType()) {
                case THUMBNAIL:
                    if (result.getThumbnail() != null) mediaHelper.delete(result.getThumbnail());
                    break;
                case INTRO_BANNER:
                    if (result.getIntroBanner() != null) mediaHelper.delete(result.getIntroBanner());
                    break;
                case BANNER:
                    if (result.getBanner() != null) mediaHelper.delete(result.getBanner());
                    break;
                default:
                    break;
            }

            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), "success");
        } catch (Exception e) {
            System.out.println(e);
            if (mediaUrl != null) mediaServer.delete(mediaUrl);
            throw new CustomInternalException(StatusCode.DB_UPLOAD_FAIL.getCode(), StatusCode.DB_UPLOAD_FAIL.getMessage(), e.getMessage());
        }
    }
}
