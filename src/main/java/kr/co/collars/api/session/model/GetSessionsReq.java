package kr.co.collars.api.session.model;

import lombok.Data;

@Data
public class GetSessionsReq {
    private Integer count;
}
