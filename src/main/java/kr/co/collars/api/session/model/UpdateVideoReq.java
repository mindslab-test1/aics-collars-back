package kr.co.collars.api.session.model;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

@Data
public class UpdateVideoReq {
    @NotNull
    private Integer id;
    private String title;
    @NotNull
    private Float duration;
    @NotNull
    private MultipartFile file;
}
