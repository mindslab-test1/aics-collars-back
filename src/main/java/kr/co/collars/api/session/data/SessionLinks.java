package kr.co.collars.api.session.data;

import lombok.Data;

@Data
public class SessionLinks {
    private String video;
    private String thumbnail;
}
