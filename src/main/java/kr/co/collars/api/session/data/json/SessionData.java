package kr.co.collars.api.session.data.json;

import lombok.Data;

import java.util.List;

@Data
public class SessionData {
    private String title;
    private Integer expiration;
    private String introduction;
    private List<String> goal;
    private SessionEffect effect;
}
