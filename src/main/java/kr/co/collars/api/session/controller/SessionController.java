package kr.co.collars.api.session.controller;

import kr.co.collars.api.session.model.UpdateDurationReq;
import kr.co.collars.api.session.model.UpdateVideoReq;
import kr.co.collars.api.session.model.UploadDataReq;
import kr.co.collars.api.session.service.SessionService;
import kr.co.collars.api.twocents.model.GetDataListReq;
import kr.co.collars.common.CommonRes;
import kr.co.collars.common.StatusCode;
import kr.co.collars.common.exception.CustomInternalException;
import kr.co.collars.common.model.GetById;
import kr.co.collars.common.model.UpdateImageReq;
import kr.co.collars.utils.exception.ValidRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static kr.co.collars.common.Types.*;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/session")
public class SessionController {
    private final SessionService sessionService;
    private final ValidRequest validRequest;

    // CREATE
    @PostMapping("/data:upload")
    public CommonRes uploadData(
            @Valid @RequestBody UploadDataReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return sessionService.uploadData(req);
    }

    // READ
    @PostMapping("/data")
    public CommonRes getData(
            @Valid @RequestBody GetById req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return sessionService.getData(req);
    }

    @PostMapping("data:list")
    public CommonRes getDataList(
            @Valid @RequestBody GetDataListReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return sessionService.getDataList(req);
    }


//    @PostMapping("file")
//    public CommonRes getSession(
//            @Valid @RequestBody GetSessionReq req,
//            BindingResult bindingResult
//    ) {
//        validRequest.check(bindingResult);
//        return sessionService.getSession(req);
//    }
//
//    @PostMapping("files")
//    public CommonRes getSessions(
//            @Valid @RequestBody GetSessionsReq req,
//            BindingResult bindingResult
//    ) {
//        validRequest.check(bindingResult);
//        return sessionService.getSessions(req);
//    }

    // UPDATE
    @PostMapping("/video:update")
    public CommonRes updateVideo(
            @Valid UpdateVideoReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return sessionService.updateVideo(req);
    }

    @PostMapping("/duraiton:update")
    public CommonRes updateDuration(
            @Valid @RequestBody UpdateDurationReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return sessionService.updateDuration(req);
    }

    @PostMapping("/image:update")
    public CommonRes updateImage(
            @Valid UpdateImageReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        String type = req.getType();
        if (type.equalsIgnoreCase(THUMBNAIL)) {
            return sessionService.updateImage(req);
        } else {
            throw new CustomInternalException(StatusCode.INVALID_PARAMETERS.getCode(), StatusCode.INVALID_PARAMETERS.getMessage(), "Type should one of [" + THUMBNAIL + ", " + INTRO_BANNER + ", " + BANNER + "]");
        }
    }
}
