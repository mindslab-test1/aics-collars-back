package kr.co.collars.api.session.service;

import kr.co.collars.api.session.data.SessionLinks;
import kr.co.collars.api.session.data.SessionRes;
import kr.co.collars.api.session.data.json.SessionEntityData;
import kr.co.collars.api.session.data.json.SessionSection;
import kr.co.collars.api.session.data.json.SessionSectionItem;
import kr.co.collars.api.session.model.UpdateDurationReq;
import kr.co.collars.api.session.model.UpdateVideoReq;
import kr.co.collars.api.session.model.UploadDataReq;
import kr.co.collars.api.twocents.model.GetDataListReq;
import kr.co.collars.common.CommonRes;
import kr.co.collars.common.StatusCode;
import kr.co.collars.common.exception.CustomInternalException;
import kr.co.collars.common.model.GetById;
import kr.co.collars.common.model.UpdateImageReq;
import kr.co.collars.dao.mapper.ExamMapper;
import kr.co.collars.dao.mapper.SessionMapper;
import kr.co.collars.dao.vo.MediaVo;
import kr.co.collars.dao.vo.UpdateMediaVo;
import kr.co.collars.dao.vo.exam.ExamMongoVo;
import kr.co.collars.dao.vo.exam.ExamVo;
import kr.co.collars.dao.vo.mongo.MongoVo;
import kr.co.collars.dao.vo.session.SessionUrlVo;
import kr.co.collars.dao.vo.session.SessionVo;
import kr.co.collars.dao.vo.user.UserSessionVo;
import kr.co.collars.mongo.entity.session.SessionEntity;
import kr.co.collars.utils.helper.AuthHelper;
import kr.co.collars.utils.helper.MediaHelper;
import kr.co.collars.utils.helper.MongoHelper;
import kr.co.collars.utils.media_server.MediaServer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static kr.co.collars.common.Types.THUMBNAIL;
import static kr.co.collars.common.Types.VIDEO;

@RequiredArgsConstructor
@Slf4j
@Transactional
@Service
public class SessionService {
    private final MediaServer mediaServer;
    private final MediaHelper mediaHelper;

    private final ExamMapper examMapper;
    private final SessionMapper sessionMapper;

    final AuthHelper authHelper;

    private final MongoTemplate mongoTemplate;
    private final MongoHelper mongoHelper;

    @Value("${media.server.url}")
    private String MEDIA_SERVER_URL;

    public CommonRes uploadData(UploadDataReq req) {
        try {
            ExamMongoVo exam = new ExamMongoVo();
            if (req.getExam() != null) {
                ExamVo examVo = new ExamVo();
                examVo.setId(req.getExam().getId());
                exam = examMapper.get(examVo);
            }

            // Mongo
            SessionEntityData sessionEntityData = new SessionEntityData(req.getData());
            sessionEntityData.getSection().getQuiz().setTitle(req.getExam().getTitle());
            if (exam.getDuration() != null) {
                sessionEntityData.getSection().getQuiz().setTime(exam.getDuration());
            } else {
                sessionEntityData.getSection().getQuiz().setTime(0.0F);
            }
            SessionEntity sessionEntity = new SessionEntity();
            sessionEntity.setData(sessionEntityData);
            mongoTemplate.save(sessionEntity);
            // Oracle(CLS_MONGO)
            MongoVo mongoVo = mongoHelper.add(mongoTemplate.getCollectionName(SessionEntity.class), sessionEntity.get_id());
            // Oracle(CLS_SESSION)
            SessionVo sessionVo = new SessionVo();
            sessionVo.setName(req.getName());
            sessionVo.setPrice(req.getPrice());
            sessionVo.setMongoId(mongoVo.getId());
            sessionVo.setExamId(req.getExam().getId());
            sessionMapper.add(sessionVo);
            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), sessionVo);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomInternalException(StatusCode.DB_UPLOAD_FAIL.getCode(), StatusCode.DB_UPLOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    // READ
    public SessionRes formattingData(SessionUrlVo data, Boolean detail) {
        SessionRes response = new SessionRes();
        response.setId(data.getId());
        response.setName(data.getName());
        response.setDuration(data.getDuration());
        response.setPrice(data.getPrice());
        response.setCreateDate(data.getCreateDate());
        response.setExamId(data.getExamId());

        SessionLinks sessionLinks = new SessionLinks();
        if (data.getVideo() != null) sessionLinks.setVideo(MEDIA_SERVER_URL + data.getVideo());
        if (data.getThumbnail() != null) sessionLinks.setThumbnail(MEDIA_SERVER_URL + data.getThumbnail());
        response.setLinks(sessionLinks);

        if (data.getMongoId() != null && detail) {
            Query query = Query.query(Criteria.where("_id").is(data.getMongoId()));
            SessionEntity entity = mongoTemplate.findOne(query, SessionEntity.class);
            response.setData(entity.getData());
        }

        return response;
    }

    public CommonRes getData(GetById req) {
        try {
            SessionVo sessionVo = new SessionVo();
            sessionVo.setId(req.getId());
            SessionUrlVo sessionUrlVo = sessionMapper.getData(sessionVo);

            SessionRes response = formattingData(sessionUrlVo, true);

            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), response);
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.DB_LOAD_FAIL.getCode(), StatusCode.DB_LOAD_FAIL.getMessage(), e.getMessage());
        }

    }

    public CommonRes getDataList(GetDataListReq req) {
        try {
            List<SessionUrlVo> sessionUrlVoList = sessionMapper.getDataList(req);
            List<SessionRes> responseList = new ArrayList<>();
            for (int i = 0; i < sessionUrlVoList.size(); i++) {
                SessionRes response = formattingData(sessionUrlVoList.get(i), req.getData());
                responseList.add(response);
            }
            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), responseList);
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.DB_LOAD_FAIL.getCode(), StatusCode.DB_LOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    public CommonRes updateVideo(UpdateVideoReq req) {
        String mediaUrl = mediaServer.upload(req.getFile());
        try {
            MediaVo mediaVo = mediaHelper.upload(req.getFile().getContentType(), mediaUrl);

            SessionVo sessionVo = new SessionVo();
            sessionVo.setId(req.getId());
            SessionVo result = sessionMapper.get(sessionVo);

            UpdateMediaVo updateMediaVo = new UpdateMediaVo();
            updateMediaVo.setId(result.getId());
            updateMediaVo.setMediaId(mediaVo.getId());
            updateMediaVo.setType(VIDEO);
            sessionMapper.updateMedia(updateMediaVo);

            String objectId = mongoHelper.getObjectId(result.getMongoId());
            Query query = Query.query(Criteria.where("_id").is(objectId));
            SessionEntity entity = mongoTemplate.findOne(query, SessionEntity.class);
            SessionSection item = entity.getData().getSection();
            SessionSectionItem video = new SessionSectionItem();
            video.setTitle(req.getTitle());
            video.setTime(req.getDuration());
            item.setVideo(video);
            mongoTemplate.save(entity);

            if (result.getVideo() != null) mediaHelper.delete(result.getVideo());

            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), "success");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            if (mediaUrl != null) mediaServer.delete(mediaUrl);
            throw new CustomInternalException(StatusCode.DB_UPLOAD_FAIL.getCode(), StatusCode.DB_UPLOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    public CommonRes updateDuration(UpdateDurationReq req) {
        int id = authHelper.getId(req.getId(), req.getUid());
        UserSessionVo userSessionVo = new UserSessionVo();
        userSessionVo.setUserId(id);
        userSessionVo.setDuration(req.getDuration());
        return null;
    }

    public CommonRes updateImage(UpdateImageReq req) {
        String mediaUrl = mediaServer.upload(req.getFile());
        try {
            MediaVo mediaVo = mediaHelper.upload(req.getFile().getContentType(), mediaUrl);

            SessionVo sessionVo = new SessionVo();
            sessionVo.setId(req.getId());
            SessionVo result = sessionMapper.get(sessionVo);

            UpdateMediaVo updateMediaVo = new UpdateMediaVo();
            updateMediaVo.setId(result.getId());
            updateMediaVo.setMediaId(mediaVo.getId());
            updateMediaVo.setType(req.getType());
            sessionMapper.updateMedia(updateMediaVo);

            switch (req.getType()) {
                case THUMBNAIL:
                    if (result.getThumbnail() != null) mediaHelper.delete(result.getThumbnail());
                    break;
            }

            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), "success");
        } catch (Exception e) {
            log.error(e.getMessage());
            if (mediaUrl != null) mediaServer.delete(mediaUrl);
            throw new CustomInternalException(StatusCode.DB_UPLOAD_FAIL.getCode(), StatusCode.DB_UPLOAD_FAIL.getMessage(), e.getMessage());
        }
    }
}
