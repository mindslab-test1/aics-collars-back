package kr.co.collars.api.session.data.json;

import lombok.Data;

@Data
public class SessionSectionItem {
    public SessionSectionItem() {
        this.title = "";
        this.time = 0.0f;
    }

    private String title;
    private Float time;
}
