package kr.co.collars.api.session.data.json;

import lombok.Data;

@Data
public class SessionSection {
    public SessionSection() {
        this.video = new SessionSectionItem();
        this.quiz = new SessionSectionItem();
    }

    private SessionSectionItem video;
    private SessionSectionItem quiz;
}
