package kr.co.collars.api.session.model;

import lombok.Data;

@Data
public class GetSessionReq {
    private Integer sessionId;
}
