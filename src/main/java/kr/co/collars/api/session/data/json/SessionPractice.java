package kr.co.collars.api.session.data.json;

import lombok.Data;

@Data
public class SessionPractice {
    private String title;
    private String subTitle;
    private Integer time;
}
