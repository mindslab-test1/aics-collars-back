package kr.co.collars.api.session.data;

import kr.co.collars.api.session.data.json.SessionEntityData;
import lombok.Data;

import java.util.Date;

@Data
public class SessionRes {
    private Integer id;
    private String name;
    private Integer duration;
    private String price;
    private Date createDate;
    private Integer examId;
    private SessionEntityData data;
    private SessionLinks links;
}
