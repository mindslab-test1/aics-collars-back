package kr.co.collars.api.session.model;

import lombok.Data;

@Data
public class Exam {
    private String title;
    private Integer id;
}
