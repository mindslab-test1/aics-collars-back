package kr.co.collars.api.session.data.json;

import lombok.Data;

@Data
public class SessionEffect {
    private Integer voca;
    private Integer grammar;
    private Integer context;
    private Integer tone;
    private Integer knowledge;
}
