package kr.co.collars.api.session.model;

import kr.co.collars.api.session.data.json.SessionData;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UploadDataReq {
    @NotNull
    private String name;
    @NotNull
    private String price;
    @NotNull
    private SessionData data;
    private Exam exam;
}
