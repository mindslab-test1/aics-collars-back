package kr.co.collars.api.session.model;

import lombok.Data;

@Data
public class UpdateDurationReq {
    private Integer id;
    private String uid;
    private Integer sessionId;
    private Integer duration;
}
