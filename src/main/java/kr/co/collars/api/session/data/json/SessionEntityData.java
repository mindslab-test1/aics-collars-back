package kr.co.collars.api.session.data.json;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class SessionEntityData {
    public SessionEntityData(SessionData data) {
        this.title = data.getTitle();
        this.expiration = data.getExpiration();
        this.introduction = data.getIntroduction();
        this.goal = data.getGoal();
        this.effect = data.getEffect();
        SessionSection sessionSection = new SessionSection();
        this.section = sessionSection;
        List<SessionPractice> sessionPracticeList = new ArrayList<>();
        this.practice = sessionPracticeList;
    }

    private String title;
    private Integer expiration;
    private String introduction;
    private List<String> goal;
    private SessionEffect effect;
    private SessionSection section;
    private List<SessionPractice> practice;
}
