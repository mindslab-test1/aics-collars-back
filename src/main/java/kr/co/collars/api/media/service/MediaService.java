package kr.co.collars.api.media.service;

import kr.co.collars.api.media.model.FileReq;
import kr.co.collars.common.CommonRes;
import kr.co.collars.common.StatusCode;
import kr.co.collars.common.exception.CustomInternalException;
import kr.co.collars.dao.vo.MediaVo;
import kr.co.collars.utils.helper.MediaHelper;
import kr.co.collars.utils.media_server.MediaServer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@RequiredArgsConstructor
@Slf4j
@Transactional
@Service
public class MediaService {
    private final MediaServer mediaServer;
    private final MediaHelper mediaHelper;

    public CommonRes imageUpload(MultipartFile file) {
        String mediaUrl = mediaServer.upload(file);
        try {
            MediaVo mediaVo = mediaHelper.upload(file.getContentType(), mediaUrl);
            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), mediaVo);
        } catch (Exception e) {
            if(mediaUrl != null) mediaServer.delete(mediaUrl);
            throw new CustomInternalException(StatusCode.UPLOAD_IMAGE_FAIL.getCode(), StatusCode.UPLOAD_IMAGE_FAIL.getMessage(), e.getMessage());
        }
    }
}
