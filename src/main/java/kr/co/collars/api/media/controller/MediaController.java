package kr.co.collars.api.media.controller;

import kr.co.collars.api.media.model.FileReq;
import kr.co.collars.api.media.service.MediaService;
import kr.co.collars.common.CommonRes;
import kr.co.collars.utils.exception.ValidRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.naming.Binding;
import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/media")
public class MediaController {
    private final MediaService mediaService;
    private final ValidRequest validRequest;

    @PostMapping("/image:upload")
    public CommonRes imageUpload(
            @Valid FileReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return mediaService.imageUpload(req.getFile());
    }

}
