package kr.co.collars.api.company.controller;

import kr.co.collars.api.company.model.CreateUserReq;
import kr.co.collars.api.company.model.UploadCompanyReq;
import kr.co.collars.api.company.service.CompanyService;
import kr.co.collars.common.CommonRes;
import kr.co.collars.utils.exception.ValidRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/company")
public class CompanyController {
    private final CompanyService companyService;
    private final ValidRequest validRequest;

    // Create
    @PostMapping("/category")
    public CommonRes uploadData(
            @Valid @RequestBody UploadCompanyReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return companyService.uploadCompany(req);
    }

    @GetMapping("/companies")
    public CommonRes getCompanies()
    {
        return companyService.getCompanies();
    }

    @PostMapping("/user:auto")
    public CommonRes createUser(
            @Valid @RequestBody CreateUserReq req,
            BindingResult bindingResult
    ){
        validRequest.check(bindingResult);
        return companyService.createUser(req);
    }
}
