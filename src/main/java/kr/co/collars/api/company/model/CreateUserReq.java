package kr.co.collars.api.company.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Getter
@Setter
public class CreateUserReq {
    @NotNull
    private Integer companyId;
    @NotNull
    private Integer numbers;
}
