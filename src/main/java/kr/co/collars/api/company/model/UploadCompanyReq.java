package kr.co.collars.api.company.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Getter
@Setter
public class UploadCompanyReq {
    @NotNull
    private String name;
    @NotNull
    private String code;
}
