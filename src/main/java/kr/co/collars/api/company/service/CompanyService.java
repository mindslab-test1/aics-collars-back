package kr.co.collars.api.company.service;

import kr.co.collars.api.company.data.CreateUserList;
import kr.co.collars.api.company.data.CreateUserRes;
import kr.co.collars.api.company.model.CreateUserReq;
import kr.co.collars.api.company.model.UploadCompanyReq;
import kr.co.collars.common.CommonRes;
import kr.co.collars.common.StatusCode;
import kr.co.collars.common.exception.CustomInternalException;
import kr.co.collars.dao.mapper.CompanyMapper;
import kr.co.collars.dao.mapper.UserMapper;
import kr.co.collars.dao.vo.company.CompanyVo;
import kr.co.collars.dao.vo.user.UserVo;
import kr.co.collars.utils.helper.PasswordHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
@Slf4j
@Transactional
@Service
public class CompanyService {
    final CompanyMapper companyMapper;
    final UserMapper userMapper;
    final PasswordHelper passwordHelper;
    final PasswordEncoder passwordEncoder;

    // Create
    public CommonRes uploadCompany(UploadCompanyReq req) {
        try {
            CompanyVo companyVo = new CompanyVo();
            companyVo.setName(req.getName());
            companyVo.setCode(req.getCode());

            companyMapper.addCompany(companyVo);
            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), companyVo);
        } catch (Exception e) {
            log.debug(e.getMessage());
            throw new CustomInternalException(StatusCode.DB_UPLOAD_FAIL.getCode(), StatusCode.DB_UPLOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    public CommonRes getCompanies(){
        try {
            List<CompanyVo> companies = companyMapper.getCompanies();
            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), companies);
        }catch (Exception e)
        {
            log.debug(e.getMessage());
            throw new CustomInternalException(StatusCode.DB_LOAD_FAIL.getCode(), StatusCode.DB_LOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    public CommonRes createUser(CreateUserReq req) {
        try {
            CompanyVo companyVo = new CompanyVo();
            companyVo.setId(req.getCompanyId());
            companyVo = companyMapper.getCompany(companyVo);
            UserVo userVo = userMapper.getLastCompanyUser(companyVo);

            Integer currentUserNumber = 0;
            if (userVo != null) {
                String[] splitUserId = userVo.getCompanyId().split("_");
                currentUserNumber = Integer.valueOf(splitUserId[splitUserId.length - 1]);
            }
            List<UserVo> userList = new ArrayList<>();

            CreateUserRes createUserRes = new CreateUserRes();
            createUserRes.setCompanyId(companyVo.getId());
            List<CreateUserList> createUserLists = new ArrayList<>();

            for (int i = 0; i < req.getNumbers(); i++) {
                currentUserNumber++;
                String id = companyVo.getName() + "_" + currentUserNumber;
                String pw = passwordHelper.generate(10);
                String encodePw = passwordEncoder.encode(pw);

                UserVo user = new UserVo();
                user.setCompany(companyVo.getId());
                user.setCompanyId(id);
                user.setCompanyPw(encodePw);
                userList.add(user);

                CreateUserList createUserList = new CreateUserList();
                createUserList.setId(id);
                createUserList.setPassword(pw);
                createUserLists.add(createUserList);
            }
            createUserRes.setUsers(createUserLists);
            System.out.println(userList);
            userMapper.signUpWithCompany(userList);

            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), createUserLists);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomInternalException(StatusCode.DB_UPLOAD_FAIL.getCode(), StatusCode.DB_UPLOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    // Read

}
