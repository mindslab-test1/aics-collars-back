package kr.co.collars.api.company.data;

import lombok.Data;

@Data
public class CreateUserList {
    private String id;
    private String password;
}
