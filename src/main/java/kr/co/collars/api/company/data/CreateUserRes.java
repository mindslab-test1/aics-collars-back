package kr.co.collars.api.company.data;

import lombok.Data;

import java.util.List;

@Data
public class CreateUserRes {
    private Integer companyId;
    private List<CreateUserList> users;
}
