package kr.co.collars.api.purchase.model;

import lombok.Data;

@Data
public class PurchaseSessionReq {
    private Integer id;
    private String uid;
    private Integer sessionId;
    private Integer expiration;
    private String purchaseToken;
}
