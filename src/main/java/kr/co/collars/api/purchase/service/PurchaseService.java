package kr.co.collars.api.purchase.service;

import kr.co.collars.api.purchase.data.UserSession;
import kr.co.collars.api.purchase.model.PurchaseSessionReq;
import kr.co.collars.common.CommonRes;
import kr.co.collars.common.StatusCode;
import kr.co.collars.common.exception.CustomInternalException;
import kr.co.collars.dao.mapper.PurchaseMapper;
import kr.co.collars.dao.vo.user.UserSessionVo;
import kr.co.collars.utils.helper.AuthHelper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Slf4j
@Transactional
@Service
public class PurchaseService {
    private final AuthHelper authHelper;
    private final PurchaseMapper purchaseMapper;

    public CommonRes purchaseSession(PurchaseSessionReq req) {
        int id = authHelper.getId(req.getId(), req.getUid());
        try {
            UserSession userSession = new UserSession();
            userSession.setUserId(id);
            userSession.setSessionId(req.getSessionId());
            userSession.setExpiryDate(req.getExpiration());
            userSession.setPurchaseToken(req.getPurchaseToken());
            UserSessionVo userSessionVo = purchaseMapper.getSession(userSession);
            if (userSessionVo != null) {
                return new CommonRes(StatusCode.ALREADY_PURCHASED.getCode(), StatusCode.ALREADY_PURCHASED.getMessage(), null);
            } else if (req.getPurchaseToken() != null) {
                purchaseMapper.addSession(userSession);
                return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), userSession);
            } else {
                return new CommonRes(StatusCode.REQUIRE_PURCHASED.getCode(), StatusCode.REQUIRE_PURCHASED.getMessage(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomInternalException(StatusCode.UPLOAD_FAIL.getCode(), StatusCode.UPLOAD_FAIL.getMessage(), e.getMessage());
        }
    }
}
