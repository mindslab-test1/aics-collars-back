package kr.co.collars.api.purchase.data;

import lombok.Data;

import java.util.Date;

@Data
public class UserSession {
    private Integer id;
    private Integer userId;
    private Integer sessionId;
    private String purchaseToken;
    private Date purchaseDate;
    private Integer expiryDate;
    private Integer duration;
}
