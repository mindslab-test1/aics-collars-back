package kr.co.collars.api.purchase.controller;

import kr.co.collars.api.purchase.model.PurchaseSessionReq;
import kr.co.collars.api.purchase.service.PurchaseService;
import kr.co.collars.common.CommonRes;
import kr.co.collars.utils.exception.ValidRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/purchase")
public class PurchaseController {
    private final PurchaseService purchaseService;
    private final ValidRequest validRequest;

    @PostMapping("/session")
    public CommonRes purcahseSession(
            @Valid @RequestBody PurchaseSessionReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return purchaseService.purchaseSession(req);
    }
}
