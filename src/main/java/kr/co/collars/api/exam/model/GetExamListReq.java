package kr.co.collars.api.exam.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Getter
@Setter
public class GetExamListReq {
    @NotNull
    private String type;
    private Integer current = 0;
    private Integer increment = 1;
    private Boolean data = false;
}
