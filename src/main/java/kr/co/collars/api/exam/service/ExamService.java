package kr.co.collars.api.exam.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import kr.co.collars.api.exam.data.ExamResult;
import kr.co.collars.api.exam.data.GetExamRes;
import kr.co.collars.api.exam.data.json.HighlightAnswer;
import kr.co.collars.api.exam.model.*;
import kr.co.collars.common.CommonRes;
import kr.co.collars.common.StatusCode;
import kr.co.collars.common.exception.CustomBadRequest;
import kr.co.collars.common.exception.CustomInternalException;
import kr.co.collars.dao.mapper.ExamMapper;
import kr.co.collars.dao.mapper.MongoMapper;
import kr.co.collars.dao.vo.exam.ExamMongoVo;
import kr.co.collars.dao.vo.exam.ExamVo;
import kr.co.collars.dao.vo.mongo.MongoVo;
import kr.co.collars.mongo.entity.exam.ExamDataEntity;
import kr.co.collars.mongo.entity.exam.ExamEntity;
import kr.co.collars.mongo.entity.user.UserExamDataEntity;
import kr.co.collars.mongo.entity.user.UserExamEntity;
import kr.co.collars.utils.helper.AuthHelper;
import kr.co.collars.utils.helper.MediaHelper;
import kr.co.collars.utils.maum.MaumServer;
import kr.co.collars.utils.maum.SimilarityServer;
import kr.co.collars.utils.media_server.MediaServer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static kr.co.collars.common.Types.*;

@RequiredArgsConstructor
@Slf4j
@Transactional
@Service
public class ExamService {

    private final MediaServer mediaServer;
    private final MediaHelper mediaHelper;
    private final MaumServer maumServer;
    private final SimilarityServer similarityServer;

    final AuthHelper authHelper;
    final ExamMapper examMapper;
    final MongoMapper mongoMapper;
    final MongoTemplate mongoTemplate;

    // CREATE
    // todo: deprecated
//    public CommonRes uploadExamFile(UploadExamFileReq req) {
//        String type = checkType(req.getType());
//        String mediaUrl = mediaServer.upload(req.getFile());
//
//        try {
//            MediaVo mediaVo = mediaHelper.upload(req.getFile().getContentType(), mediaUrl);
//
//            ExamVo examVo = new ExamVo();
//            examVo.setType(type);
//            if (req.getName() != null) examVo.setName(req.getName());
//            else examVo.setName(FilenameUtils.getBaseName(req.getFile().getOriginalFilename()));
//            examVo.setData(mediaVo.getId());
//            examMapper.uploadExam(examVo);
//            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), examVo);
//        } catch (Exception e) {
//            mediaServer.delete(mediaUrl);
//            throw new CustomInternalException(StatusCode.DB_UPLOAD_FAIL.getCode(), StatusCode.DB_UPLOAD_FAIL.getMessage(), e.getMessage());
//        }
//    }

    public CommonRes uploadExamData(UploadExamDataReq req) {
        try {
            // Mongo
            mongoTemplate.save(req.getExam());
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.MONGO_UPLOAD_FAIL.getCode(), StatusCode.MONGO_UPLOAD_FAIL.getMessage(), e.getMessage());
        }
        try {
            // Oracle
            MongoVo mongoVo = new MongoVo();
            mongoVo.setCollection(mongoTemplate.getCollectionName(ExamEntity.class));
            mongoVo.setObjectId(req.getExam().get_id());
            mongoMapper.add(mongoVo);
            // Oracle
            ExamVo examVo = new ExamVo();
            examVo.setType(req.getExam().getType());
            examVo.setName(req.getName());
            examVo.setDuration(req.getDuration());
            examVo.setData(mongoVo.getId());
            examMapper.uploadExam(examVo);

            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), examVo);
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.DB_UPLOAD_FAIL.getCode(), StatusCode.DB_UPLOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    public CommonRes uploadExamResult(UploadExamResultReq req) {
        GetExamReq getExamReq = new GetExamReq();
        getExamReq.setId(req.getExamId());
        GetExamRes exam = (GetExamRes) getExam(getExamReq).getPayload();
        List<ExamDataEntity> examData = exam.getData();

        UserExamEntity userExam = new UserExamEntity();
        int userId = authHelper.getId(req.getId(), req.getUid());
        userExam.setUserId(userId);
        userExam.setExamId(req.getExamId());
        userExam.setType(req.getType());
        List<UserExamDataEntity> userExamDataList = new ArrayList<>();

        try {
            for (ExamResult result : req.getResult()) {
                UserExamDataEntity userExamData = new UserExamDataEntity();
                userExamData.setType(result.getType());
                switch (result.getType()) {
                    case CHOICE:
                        for (ExamDataEntity content : examData) {
                            if (content.getType().equalsIgnoreCase(CHOICE)) {
                                if (result.getResult() == content.getAnswer().get(0)) {
                                    userExamData.setScore(100);
                                } else {
                                    userExamData.setScore(0);
                                }
                                break;
                            }
                        }
                        if (userExamData.getScore() == null) userExamData.setScore(0);
                        break;
                    case PUZZLE:
                        for (ExamDataEntity content : examData) {
                            if (content.getType().equalsIgnoreCase(PUZZLE)) {
                                int count = 0;
                                List<String> res = (List<String>) result.getResult();
                                for (int i = 0; i < content.getAnswer().size(); i++) {
                                    if (res.get(i).equalsIgnoreCase(content.getAnswer().get(i).toString())) {
                                        count++;
                                    }
                                }
                                double score = (float) count / (float) res.size() * 100.0;
                                userExamData.setScore((int) score);
                            }
                        }
                        if (userExamData.getScore() == null) userExamData.setScore(0);
                        break;
                    case SPEAKING:
                        for (ExamDataEntity content : examData) {
                            if (content.getType().equalsIgnoreCase(SPEAKING)) {
                                String userAnswer = (String) result.getResult();
                                List<String> answers = content.getAnswer().stream().map(Object::toString).collect(Collectors.toList());
                                similarityServer.setDestination();
                                double score = similarityServer.GetScore(userAnswer, answers) * 100;
                                userExamData.setScore((int) score);
                                break;
                            }
                        }
                        if (userExamData.getScore() == null) userExamData.setScore(0);
                        break;
                    case SHORT_ANSWER:
                        for (ExamDataEntity content : examData) {
                            if (content.getType().equalsIgnoreCase(SHORT_ANSWER)) {
                                String userAnswer = (String) result.getResult();
                                List<String> answers = content.getAnswer().stream().map(Object::toString).collect(Collectors.toList());
                                similarityServer.setDestination();
                                double score = similarityServer.GetScore(userAnswer, answers) * 100;
                                userExamData.setScore((int) score);
                                break;
                            }
                        }
                        if (userExamData.getScore() == null) userExamData.setScore(0);
                        break;
                    case HIGHLIGHT:
                        for (ExamDataEntity content : examData) {
                            if (content.getType().equalsIgnoreCase(HIGHLIGHT)) {

                                int count = 0;
                                int correct = 0;

                                ObjectMapper mapper = new ObjectMapper();
                                List<List<HighlightAnswer>> answers = mapper.convertValue(content.getAnswer(), new TypeReference<List<List<HighlightAnswer>>>() {
                                });
                                List<List<HighlightAnswer>> userAnswers = mapper.convertValue(result.getResult(), new TypeReference<List<List<HighlightAnswer>>>() {
                                });


                                for (int i = 0; i < answers.size(); i++) {
                                    List<HighlightAnswer> answer = answers.get(i);
                                    for (int j = 0; j < answer.size(); j++) {
                                        if (answer.get(j).getIs_answer()) {
                                            count++;
                                            if (userAnswers.get(i).get(j).getIs_answer()) correct++;
                                        } else {
                                            if (userAnswers.get(i).get(j).getIs_answer()) correct--;
                                        }
                                    }
                                }
                                double score = 100 * correct / count;
                                if (score < 0) score = 0;
                                userExamData.setScore((int) score);
                                if (userExamData.getScore() == null) userExamData.setScore(0);
                                break;
                            }
                        }
                        break;
                }
                userExamDataList.add(userExamData);
            }
            userExam.setData(userExamDataList);
            mongoTemplate.save(userExam);
//            examMapper.uploadResult(userExamVo);
            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), userExam);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            throw new CustomInternalException(StatusCode.DB_LOAD_FAIL.getCode(), StatusCode.DB_LOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    public CommonRes uploadExamAudio(UploadExamAudioReq req) {
        try {
//            String result = maumServer.cnnStt(req.getFile());
            String result = maumServer.cnnSttDetail(req.getFile());
            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), result);
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.MAUM_SERVER_FAIL.getCode(), StatusCode.MAUM_SERVER_FAIL.getMessage(), e.getMessage());
        }
    }

    // READ
    // todo: deprecated
//    public CommonRes getExam(GetExamReq req) {
//        String type = checkType(req.getType());
//
//        try {
//            ExamVo examVo = new ExamVo();
//            examVo.setType(type);
//            ExamUrlVo selectedExam;
//            if (req.getId() != null) {
//                examVo.setId(req.getId());
//                selectedExam = examMapper.getExam(examVo);
//            } else {
//                List<ExamUrlVo> exams = examMapper.getExams(examVo);
//                Random rand = new Random();
//                selectedExam = exams.get(rand.nextInt(exams.size()));
//            }
//            GetExamRes getExamRes = new GetExamRes();
//            getExamRes.setId(selectedExam.getId());
//            getExamRes.setType(selectedExam.getType());
//            getExamRes.setName(selectedExam.getName());
//            ExamData data = (ExamData) mediaServer.get(selectedExam.getData(), ExamData.class);
//            getExamRes.setData(data);
//            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), getExamRes);
//        } catch (Exception e) {
//            throw new CustomInternalException(StatusCode.DB_LOAD_FAIL.getCode(), StatusCode.DB_LOAD_FAIL.getMessage(), e.getMessage());
//        }
//    }


    public CommonRes getExam(GetExamReq req) {
        try {
            ExamVo examVo = new ExamVo();
            examVo.setId(req.getId());
            ExamMongoVo examMongoVo = examMapper.get(examVo);

            Query query = Query.query(Criteria.where("_id").is(examMongoVo.getObjectId()));
            ExamEntity exam = mongoTemplate.findOne(query, ExamEntity.class);

            GetExamRes res = new GetExamRes();
            res.setId(examMongoVo.getId());
            res.setType(examMongoVo.getType());
            res.setName(examMongoVo.getName());
            res.setData(exam.getData());
            res.setDuration(examMongoVo.getDuration());

            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), res);
        } catch (Exception e) {
            log.debug(e.getMessage());
            throw new CustomInternalException(StatusCode.DB_LOAD_FAIL.getCode(), StatusCode.DB_LOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    public CommonRes getExamList(GetExamListReq req) {
        try {
            List<ExamMongoVo> examMongoVoList = examMapper.getList(req);
            List<GetExamRes> res = new ArrayList<>();
            for (ExamMongoVo examMongoVo : examMongoVoList) {
                GetExamRes getExamRes = new GetExamRes();
                getExamRes.setId(examMongoVo.getId());
                getExamRes.setType(examMongoVo.getType());
                getExamRes.setName(examMongoVo.getName());
                getExamRes.setDuration(examMongoVo.getDuration());
                if (req.getData()) {
                    Query query = Query.query(Criteria.where("_id").is(examMongoVo.getObjectId()));
                    ExamEntity exam = mongoTemplate.findOne(query, ExamEntity.class);
                    getExamRes.setData(exam.getData());
                }
                res.add(getExamRes);
            }

            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), res);
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.DB_LOAD_FAIL.getCode(), StatusCode.DB_LOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    public CommonRes getExamTypes() {
        List<String> types = examMapper.getTypes();
        return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), types);
    }

    // Function
    private String checkType(String type) {
        switch (type.toUpperCase()) {
            case LEVEL:
                return LEVEL;
            case SESSION:
                return SESSION;
            default:
                throw new CustomBadRequest(StatusCode.INVALID_PARAMETERS.getCode(), StatusCode.INVALID_PARAMETERS.getMessage(), "Type should one of session, level");
        }
    }
}
