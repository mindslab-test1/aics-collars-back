package kr.co.collars.api.exam.model;

import kr.co.collars.mongo.entity.exam.ExamEntity;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class UploadExamDataReq {
    private String name;
    private ExamEntity exam;
    private Float duration;
}
