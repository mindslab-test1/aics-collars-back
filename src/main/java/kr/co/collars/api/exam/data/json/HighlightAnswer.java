package kr.co.collars.api.exam.data.json;


public class HighlightAnswer {
    private String word;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }
    public Boolean getIs_answer() {
        return is_answer;
    }

    public void setIs_answer(Boolean is_answer) {
        this.is_answer = is_answer;
    }
    private Boolean is_answer;

    @Override
    public String toString() {
        return "HighlightAnswer{" +
                "word='" + word + '\'' +
                ", is_answer=" + is_answer +
                '}';
    }
}
