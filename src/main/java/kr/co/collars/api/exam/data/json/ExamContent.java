package kr.co.collars.api.exam.data.json;

import lombok.Data;

import java.util.List;

@Data
public class ExamContent {
    private String type;
    private String question;
    private List<String> subQuestion;
    private List<Object> answer;
}
