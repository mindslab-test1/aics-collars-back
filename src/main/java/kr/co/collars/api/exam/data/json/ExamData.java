package kr.co.collars.api.exam.data.json;

import lombok.Data;

import java.util.List;

@Data
public class ExamData {
    private String type;
    private List<ExamContent> data;
}
