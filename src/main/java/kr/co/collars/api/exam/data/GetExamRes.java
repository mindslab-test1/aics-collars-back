package kr.co.collars.api.exam.data;

import kr.co.collars.api.exam.data.json.ExamData;
import kr.co.collars.mongo.entity.exam.ExamDataEntity;
import kr.co.collars.mongo.entity.exam.ExamEntity;
import lombok.Data;

import java.util.List;

@Data
public class GetExamRes {
    private Integer id;
    private String type;
    private String name;
    private List<ExamDataEntity> data;
    private Float duration;
}
