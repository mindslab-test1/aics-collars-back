package kr.co.collars.api.exam.model;

import kr.co.collars.api.exam.data.ExamResult;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class UploadExamResultReq {
    private Integer id;
    private String uid;
    @NotNull
    private String type;
    @NotNull
    private Integer examId;
    @NotNull
    private List<ExamResult> result;
}
