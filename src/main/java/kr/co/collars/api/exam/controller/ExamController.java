package kr.co.collars.api.exam.controller;

import kr.co.collars.api.exam.model.*;
import kr.co.collars.api.exam.service.ExamService;
import kr.co.collars.common.CommonRes;
import kr.co.collars.utils.exception.ValidRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/exam")
public class ExamController {
    private final ExamService examService;
    private final ValidRequest validRequest;

    // CREATE
    // todo: deprecated
//    @PostMapping("/file:upload")
//    public CommonRes uploadExamFile(
//            @Valid UploadExamFileReq req,
//            BindingResult bindingResult
//    ) {
//        validRequest.check(bindingResult);
//        return examService.uploadExamFile(req);
//    }

    @PostMapping("data:upload")
    public CommonRes uploadExamData(
            @Valid @RequestBody UploadExamDataReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return examService.uploadExamData(req);
    }

    @PostMapping("/result:upload")
    public CommonRes uploadExamResult(
            @Valid @RequestBody UploadExamResultReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return examService.uploadExamResult(req);
    }

    @PostMapping("/audio")
    public CommonRes uploadExamAudio(
            @Valid UploadExamAudioReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return examService.uploadExamAudio(req);
    }

    // READ
    @GetMapping("/types")
    public CommonRes getExamTypes(){
        return examService.getExamTypes();
    }

    @PostMapping("/data")
    public CommonRes getExam(
            @Valid @RequestBody GetExamReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return examService.getExam(req);
    }


    @PostMapping("/data:list")
    public CommonRes getExams(
            @Valid @RequestBody GetExamListReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return examService.getExamList(req);
    }

}
