package kr.co.collars.api.exam.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Getter
@Setter
public class UploadExamFileReq {
    @NotBlank
    private String type;
    private String name;
    @NotNull
    private MultipartFile file;
}
