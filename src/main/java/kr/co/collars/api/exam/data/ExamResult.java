package kr.co.collars.api.exam.data;

import lombok.Data;

@Data
public class ExamResult {
    private String type;
    private Object result;
}
