package kr.co.collars.api.auth.data;

import kr.co.collars.api.session.data.SessionRes;
import kr.co.collars.dao.vo.session.SessionUrlVo;
import kr.co.collars.mongo.entity.user.UserExamEntity;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class UserData {
    private String email;
    private String name;
    private Date signUpDate;
    private Date signInDate;
    private boolean google;
    private boolean naver;
    private boolean kakao;
    private boolean facebook;
    private boolean password;
    private List<UserSurvey> survey;
    private List<UserExamEntity> exam;
    private List<SessionRes> session;
    private String company;
    private String companyId;
}
