package kr.co.collars.api.auth.data;

import lombok.Data;

@Data
public class UserExam {
    private Integer examId;
    private String examName;
    private String examType;
    private Integer choice;
    private Integer puzzle;
    private Integer speaking;
    private Integer shortAnswer;
    private Integer highlight;
    private Data createDate;
}
