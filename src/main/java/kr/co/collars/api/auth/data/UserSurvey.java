package kr.co.collars.api.auth.data;

import lombok.Data;

@Data
public class UserSurvey {
    private Integer questionId;
    private Integer answerId;
    private String question;
    private String answer;
}
