package kr.co.collars.api.auth.service;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.UserRecord;
import kr.co.collars.api.auth.data.UserData;
import kr.co.collars.api.auth.data.UserSurvey;
import kr.co.collars.api.auth.model.*;
import kr.co.collars.api.session.data.SessionRes;
import kr.co.collars.api.session.service.SessionService;
import kr.co.collars.common.CommonRes;
import kr.co.collars.common.StatusCode;
import kr.co.collars.common.Types;
import kr.co.collars.common.exception.CustomBadRequest;
import kr.co.collars.common.exception.CustomInternalException;
import kr.co.collars.dao.mapper.*;
import kr.co.collars.dao.vo.session.SessionUrlVo;
import kr.co.collars.dao.vo.user.UserVo;
import kr.co.collars.mongo.entity.user.UserExamEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Transactional
@Service
public class AuthService {

    final UserMapper userMapper;
    final SurveyMapper surveyMapper;
    final ExamMapper examMapper;
    final SessionMapper sessionMapper;
    final CompanyMapper companyMapper;

    final SessionService sessionService;
    final MongoTemplate mongoTemplate;
    final PasswordEncoder passwordEncoder;

    public AuthService(UserMapper userMapper, SurveyMapper surveyMapper, ExamMapper examMapper, SessionMapper sessionMapper, CompanyMapper companyMapper, SessionService sessionService, MongoTemplate mongoTemplate, PasswordEncoder passwordEncoder) {
        // todo: @Configuration
        try {
            ClassPathResource serviceAccout = new ClassPathResource("key/serviceAccountKey.json");
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccout.getInputStream()))
                    .build();
            FirebaseApp.initializeApp(options);
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.FIREBASE_INIT_FAIL.getCode(), "AuthService[" + StatusCode.FIREBASE_INIT_FAIL.getMessage() + "]", e.getMessage());
        }
        this.userMapper = userMapper;
        this.surveyMapper = surveyMapper;
        this.examMapper = examMapper;
        this.sessionMapper = sessionMapper;
        this.sessionService = sessionService;
        this.mongoTemplate = mongoTemplate;
        this.companyMapper = companyMapper;

        this.passwordEncoder = passwordEncoder;
    }

    // Sign In
    public CommonRes validUser(AuthReq req) {
        UserRecord userRecord = getUserRecord(req);
        if (userRecord != null) {
            validUserWithDB(userRecord, req.getProvider());
            switch (req.getProvider()) {
                case Types.PASSWORD:
                case Types.GOOGLE:
                case Types.FACEBOOK:
                    return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), true);
                case Types.NAVER:
                case Types.KAKAO:
                    try {
                        String uid = FirebaseAuth.getInstance().createCustomToken(userRecord.getUid());
                        return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), uid);
                    } catch (FirebaseAuthException e) {
                        throw new CustomInternalException(StatusCode.FIREBASE_USER_LOAD_FAIL.getCode(), StatusCode.FIREBASE_USER_LOAD_FAIL.getMessage(), e.getMessage());
                    }
                default:
                    throw new CustomBadRequest(StatusCode.INVALID_PARAMETERS.getCode(), StatusCode.FIREBASE_INIT_FAIL.getMessage(), "provider should one of GOOGLE, FACEBOOK, KAKAO, NAVER");
            }
        } else {
            throw new CustomInternalException(StatusCode.FIREBASE_USER_LOAD_FAIL.getCode(), StatusCode.FIREBASE_USER_LOAD_FAIL.getMessage(), null);
        }
    }

    public CommonRes validCompanyUser(AuthCompanyReq req) {
        UserVo user = userMapper.getCompanyUser(req.getCode(), req.getId());
        boolean isMatch = passwordEncoder.matches(req.getPw(), user.getCompanyPw());
        if (isMatch) {
            userMapper.updateLastSignIn(user);

            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), user.getId());
        } else {
            return new CommonRes(StatusCode.SIGN_IN_FAIL.getCode(), StatusCode.SIGN_IN_FAIL.getMessage(), null);
        }
    }

    // READ
    public CommonRes getUser(GetUserReq req) {
        try {
            UserRecord userRecord = FirebaseAuth.getInstance().getUser(req.getUid());
            UserVo userVo = new UserVo();
            userVo.setEmail(userRecord.getEmail());
            UserVo user = userMapper.validUser(userVo);

            UserData data = getUserData(user);
            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), data);
        } catch (FirebaseAuthException e) {
            log.error(e.getMessage());
            throw new CustomInternalException(StatusCode.FIREBASE_USER_LOAD_FAIL.getCode(), StatusCode.FIREBASE_USER_LOAD_FAIL.getMessage(), e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new CustomInternalException(StatusCode.DB_LOAD_FAIL.getCode(), StatusCode.DB_LOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    public CommonRes getCompanyUser(GetCompanyUserReq req) {
        try {
            UserVo userVo = new UserVo();
            userVo.setId(req.getId());
            UserData data = getUserData(userVo);
            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), data);
        } catch (Exception e) {
            log.debug(e.getMessage());
            throw new CustomInternalException(StatusCode.DB_LOAD_FAIL.getCode(), StatusCode.DB_LOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    // Update
    public CommonRes updateUserName(UpdateUserNameReq req) {
        try {
            int id = 0;
            if (req.getUid() != null) {
                UserRecord userRecord = FirebaseAuth.getInstance().getUser(req.getUid());
                id = getUserId(userRecord.getUid());
            } else {
                id = req.getId();
            }
            UserVo userVo = new UserVo();
            userVo.setId(id);
            userVo.setName(req.getName());
            userMapper.updateName(userVo);
            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), true);
        } catch (FirebaseAuthException e) {
            throw new CustomInternalException(StatusCode.FIREBASE_USER_LOAD_FAIL.getCode(), StatusCode.FIREBASE_USER_LOAD_FAIL.getMessage(), e.getMessage());
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.DB_UPDATE_USER_FAIL.getCode(), StatusCode.DB_UPDATE_USER_FAIL.getMessage(), e.getMessage());
        }
    }

    // Function
    // @desc: Get UserRecord from firebase
    public int getUserId(String uid) {
        try {
            UserRecord userRecord = FirebaseAuth.getInstance().getUser(uid);
            UserVo userVo = new UserVo();
            userVo.setEmail(userRecord.getEmail());
            UserVo data = userMapper.validUser(userVo);

            return data.getId();
        } catch (FirebaseAuthException e) {
            throw new CustomInternalException(StatusCode.FIREBASE_USER_LOAD_FAIL.getCode(), StatusCode.FIREBASE_USER_LOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    private UserData getUserData(UserVo user) {
        try {
            UserData data = userMapper.getUser(user);
            List<UserSurvey> survey = surveyMapper.getUserSurveyAll(user);
            data.setSurvey(survey);
            Query query = Query.query(Criteria.where("userId").is(user.getId()));
            List<UserExamEntity> userExamList = mongoTemplate.find(query, UserExamEntity.class);
            data.setExam(userExamList);
            List<SessionUrlVo> sessions = sessionMapper.getUserData(user);
            List<SessionRes> sessionResList = new ArrayList<>();
            for (SessionUrlVo session : sessions) {
                sessionResList.add(sessionService.formattingData(session, true));
            }
            data.setSession(sessionResList);
            return data;
        }catch (Exception e){
            log.error(e.getMessage());
            throw new CustomInternalException(StatusCode.DB_LOAD_FAIL.getCode(), StatusCode.DB_LOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    public UserRecord getUserRecord(AuthReq req) {
        UserRecord userRecord = null;
        try {
            userRecord = FirebaseAuth.getInstance().getUserByEmail(req.getEmail());
        } catch (FirebaseAuthException e) {
            // @desc: Automatically sign in on firebaseAuth
            if (e.getErrorCode().equals("user-not-found")) {
                UserRecord.CreateRequest newUser = new UserRecord.CreateRequest();
                newUser.setEmail(req.getEmail());
                newUser.setDisplayName(req.getName());
                try {
                    userRecord = FirebaseAuth.getInstance().createUser(newUser);
                } catch (FirebaseAuthException e1) {
                    throw new CustomInternalException(StatusCode.FIREBASE_USER_LOAD_FAIL.getCode(), StatusCode.FIREBASE_USER_LOAD_FAIL.getMessage(), e1);
                }
            } else {
                throw new CustomInternalException(StatusCode.FIREBASE_USER_LOAD_FAIL.getCode(), StatusCode.FIREBASE_USER_LOAD_FAIL.getMessage(), e);
            }
        }
        return userRecord;
    }

    public void validUserWithDB(UserRecord userRecord, String provider) {
        UserVo userVo = new UserVo();
        userVo.setEmail(userRecord.getEmail());
        UserVo userData = userMapper.validUser(userVo);
        try {
            if (userData == null) {
                createUser(userRecord, provider);
                userData = userMapper.validUser(userVo);
            }

            switch (provider) {
                case Types.PASSWORD:
                    if (userData.getPassword() != 1) userMapper.updateProvider(provider, userData.getEmail());
                    break;
                case Types.GOOGLE:
                    if (userData.getGoogle() != 1) userMapper.updateProvider(provider, userData.getEmail());
                    break;
                case Types.FACEBOOK:
                    if (userData.getFacebook() != 1) userMapper.updateProvider(provider, userData.getEmail());
                    break;
                case Types.NAVER:
                    if (userData.getNaver() != 1) userMapper.updateProvider(provider, userData.getEmail());
                    break;
                case Types.KAKAO:
                    if (userData.getKakao() != 1) userMapper.updateProvider(provider, userData.getEmail());
                    break;
            }

            userMapper.updateLastSignIn(userData);
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.DB_VALID_FAIL.getCode(), StatusCode.DB_VALID_FAIL.getMessage(), e.getMessage());
        }
    }

    // @desc: DB CREATE
    public void createUser(UserRecord userRecord, String provider) {
        UserVo userVo = new UserVo();
        userVo.setEmail(userRecord.getEmail());
        userVo.setName(userRecord.getDisplayName());
        userVo.setPassword(0);
        userVo.setGoogle(0);
        userVo.setFacebook(0);
        userVo.setNaver(0);
        userVo.setKakao(0);
        switch (provider) {
            case Types.PASSWORD:
                userVo.setPassword(1);
                break;
            case Types.GOOGLE:
                userVo.setGoogle(1);
                break;
            case Types.FACEBOOK:
                userVo.setFacebook(1);
                break;
            case Types.NAVER:
                userVo.setNaver(1);
                break;
            case Types.KAKAO:
                userVo.setKakao(1);
                break;
            default:
                throw new CustomBadRequest(StatusCode.INVALID_PARAMETERS.getCode(), StatusCode.FIREBASE_INIT_FAIL.getMessage(), "provider should one of GOOGLE, FACEBOOK, KAKAO, NAVER");
        }

        try {
            userMapper.signUpWithProvider(userVo);
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.DB_CREATE_USER_FAIL.getCode(), StatusCode.DB_CREATE_USER_FAIL.getMessage(), e.getMessage());
        }
    }
}
