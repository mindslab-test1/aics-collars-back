package kr.co.collars.api.auth.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Getter
@Setter
public class AuthCompanyReq {
    @NotNull
    private String code;
    @NotNull
    private String id;
    @NotNull
    private String pw;
}
