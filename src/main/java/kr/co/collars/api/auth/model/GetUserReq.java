package kr.co.collars.api.auth.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@NoArgsConstructor
@Getter
@Setter
public class GetUserReq {
    @NotBlank
    private String uid;
}
