package kr.co.collars.api.auth.model;

import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.*;

@NoArgsConstructor
@Getter
@Setter
public class AuthReq {
    @NotNull
    private String provider;
    @NotBlank
    @Email
    private String email;
    private String password;
    private String name;
}
