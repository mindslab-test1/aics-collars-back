package kr.co.collars.api.auth.controller;

import kr.co.collars.api.auth.model.*;
import kr.co.collars.api.auth.service.AuthService;
import kr.co.collars.common.CommonRes;
import kr.co.collars.utils.exception.ValidRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/auth")
public class AuthController {
    private final AuthService authService;
    private final ValidRequest validRequest;

    // @desc: Check user is exist in the DB (for Google)
    @PostMapping("/validUser")
    public CommonRes validUser(
            @Valid @RequestBody AuthReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return authService.validUser(req);
    }

    @PostMapping("/validCompanyUser")
    public CommonRes validCompanyUser(
            @Valid @RequestBody AuthCompanyReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return authService.validCompanyUser(req);

    }

    @PostMapping("/user")
    public CommonRes getUser(
            @Valid @RequestBody GetUserReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return authService.getUser(req);
    }

    @PostMapping("/userById")
    public CommonRes getCompanyUser(
            @Valid @RequestBody GetCompanyUserReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return authService.getCompanyUser(req);
    }

    // @desc: Update user name
    @PatchMapping("/user/name")
    public CommonRes updateUserName(
            @Valid @RequestBody UpdateUserNameReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return authService.updateUserName(req);
    }
}
