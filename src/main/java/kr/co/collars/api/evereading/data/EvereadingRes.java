package kr.co.collars.api.evereading.data;

import kr.co.collars.api.evereading.data.json.EvereadingData;
import kr.co.collars.common.data.Lang;
import lombok.Data;

import java.util.Date;

@Data
public class EvereadingRes {
    private Integer id;
    private String name;
    private Lang sector;
    private Integer lev;
    private Date createDate;
    private EvereadingData data;
    private EvereadingLinks links;
}
