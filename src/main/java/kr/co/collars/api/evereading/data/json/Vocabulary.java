package kr.co.collars.api.evereading.data.json;

import lombok.Data;

@Data
public class Vocabulary {
    private Integer index;
    private String eng;
    private String kor;
}
