package kr.co.collars.api.evereading.data;

import lombok.Data;

@Data
public class EvereadingLinks {
    private String thumbnail;
    private String introBanner;
    private String banner;
}
