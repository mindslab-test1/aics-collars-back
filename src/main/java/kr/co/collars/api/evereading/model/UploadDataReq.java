package kr.co.collars.api.evereading.model;

import kr.co.collars.api.evereading.data.json.EvereadingData;
import kr.co.collars.common.data.Lang;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Getter
@Setter
public class UploadDataReq {
    @NotNull
    private String name;
    @NotNull
    private Lang sector;
    @NotNull
    private Integer level;
    @NotNull
    private EvereadingData data;
}
