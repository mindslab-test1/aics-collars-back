package kr.co.collars.api.evereading.data.json;

import java.util.List;

@lombok.Data
public class Data {
    private String title;
    private String description;
    private List<Content> content;
}
