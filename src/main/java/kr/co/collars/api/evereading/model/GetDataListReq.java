package kr.co.collars.api.evereading.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Getter
@Setter
public class GetDataListReq {
    @NotNull
    private Integer tagId;
    @NotNull
    private Integer current;
    @NotNull
    private Integer increment;
    private Boolean data = false;
}
