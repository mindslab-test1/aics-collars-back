package kr.co.collars.api.evereading.service;

import kr.co.collars.api.evereading.data.EvereadingLinks;
import kr.co.collars.api.evereading.data.EvereadingRes;
import kr.co.collars.api.evereading.model.GetDataListReq;
import kr.co.collars.api.evereading.model.GetSectorsReq;
import kr.co.collars.api.evereading.model.UploadDataReq;
import kr.co.collars.common.CommonRes;
import kr.co.collars.common.StatusCode;
import kr.co.collars.common.data.Lang;
import kr.co.collars.common.exception.CustomBadRequest;
import kr.co.collars.common.exception.CustomInternalException;
import kr.co.collars.common.model.GetById;
import kr.co.collars.common.model.UpdateImageReq;
import kr.co.collars.dao.mapper.EvereadingMapper;
import kr.co.collars.dao.mapper.MongoMapper;
import kr.co.collars.dao.vo.HashTagVo;
import kr.co.collars.dao.vo.MediaVo;
import kr.co.collars.dao.vo.UpdateMediaVo;
import kr.co.collars.dao.vo.evereading.EvereadingUrlVo;
import kr.co.collars.dao.vo.evereading.EvereadingVo;
import kr.co.collars.dao.vo.mongo.MongoVo;
import kr.co.collars.mongo.entity.evereading.EvereadingEntity;
import kr.co.collars.mongo.entity.TwoCentsEntity;
import kr.co.collars.utils.helper.HashTagHelper;
import kr.co.collars.utils.helper.MediaHelper;
import kr.co.collars.utils.media_server.MediaServer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static kr.co.collars.common.Types.*;

@RequiredArgsConstructor
@Slf4j
@Transactional
@Service
public class EvereadingService {
    private final MediaServer mediaServer;
    private final MediaHelper mediaHelper;
    private final HashTagHelper hashTagHelper;

    final EvereadingMapper evereadingMapper;

    final MongoMapper mongoMapper;
    final MongoTemplate mongoTemplate;

    @Value("${media.server.url}")
    private String MEDIA_SERVER_URL;

    // Upload
    public CommonRes uploadData(UploadDataReq req) {
        try {
            //Mongo
            EvereadingEntity evereadingEntity = new EvereadingEntity();
            evereadingEntity.setData(req.getData());
            mongoTemplate.save(evereadingEntity);
            // Oracle(CLS_MONGO)
            MongoVo mongoVo = new MongoVo();
            mongoVo.setCollection(mongoTemplate.getCollectionName(TwoCentsEntity.class));
            mongoVo.setObjectId(evereadingEntity.get_id());
            mongoMapper.add(mongoVo);
            // Oracle(CLS_EVEREADING)
            HashTagVo sectorKor = hashTagHelper.get(req.getSector().getKor());
            HashTagVo sectorEng = hashTagHelper.get(req.getSector().getEng());
            EvereadingVo evereadingVo = new EvereadingVo();
            evereadingVo.setName(req.getName());
            evereadingVo.setSectorEng(sectorEng.getId());
            evereadingVo.setSectorKor(sectorKor.getId());
            evereadingVo.setLev(req.getLevel());
            evereadingVo.setMongoId(mongoVo.getId());
            evereadingMapper.add(evereadingVo);
            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), evereadingVo);
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.DB_UPLOAD_FAIL.getCode(), StatusCode.DB_UPLOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    // Read
    private EvereadingRes formattingData(EvereadingUrlVo data, Boolean detail) {
        EvereadingRes response = new EvereadingRes();
        response.setId(data.getId());
        response.setName(data.getName());
        response.setLev(data.getLev());
        response.setCreateDate(data.getCreateDate());

        Lang sector = new Lang();
        sector.setEng(data.getSectorEng());
        sector.setKor(data.getSectorKor());
        response.setSector(sector);

        EvereadingLinks evereadingLinks = new EvereadingLinks();
        if (data.getThumbnail() != null)
            evereadingLinks.setThumbnail(MEDIA_SERVER_URL + data.getThumbnail());
        if (data.getIntroBanner() != null)
            evereadingLinks.setIntroBanner(MEDIA_SERVER_URL + data.getIntroBanner());
        if (data.getBanner() != null)
            evereadingLinks.setBanner(MEDIA_SERVER_URL + data.getBanner());
        response.setLinks(evereadingLinks);

        if (data.getMongoId() != null && detail) {
            Query query = Query.query(Criteria.where("_id").is(data.getMongoId()));
            EvereadingEntity entity = mongoTemplate.findOne(query, EvereadingEntity.class);
            response.setData(entity.getData());
        }

        return response;
    }

    public CommonRes getData(GetById req) {
        try {
            EvereadingVo evereadingVo = new EvereadingVo();
            evereadingVo.setId(req.getId());
            EvereadingUrlVo evereadingUrlVo = evereadingMapper.getData(evereadingVo);

            EvereadingRes response = formattingData(evereadingUrlVo, true);

            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), response);
        }catch (Exception e){
            throw new CustomInternalException(StatusCode.DB_LOAD_FAIL.getCode(), StatusCode.DB_LOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    public CommonRes getDataList(GetDataListReq req) {
        try {
            List<EvereadingUrlVo> evereadingUrlVoList = evereadingMapper.getDataList(req);
            List<EvereadingRes> responseList = new ArrayList<>();
            for (int i = 0; i < evereadingUrlVoList.size(); i++) {
                EvereadingRes response = formattingData(evereadingUrlVoList.get(i), req.getData());
                responseList.add(response);
            }
            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), responseList);
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.DB_LOAD_FAIL.getCode(), StatusCode.DB_LOAD_FAIL.getMessage(), e.getMessage());
        }
    }

    public CommonRes getSectors(GetSectorsReq req) {
        if (!req.getLang().equalsIgnoreCase("kor") && !req.getLang().equalsIgnoreCase("eng")) {
            throw new CustomBadRequest(StatusCode.INVALID_PARAMETERS.getCode(), StatusCode.INVALID_PARAMETERS.getMessage(), "Lang parameter should kor or eng");
        }
        List<HashTagVo> result = evereadingMapper.getSectors(req.getLang());
        return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), result);
    }

    // Update
    public CommonRes updateImage(UpdateImageReq req) {
        String mediaUrl = mediaServer.upload(req.getFile());
        try {
            MediaVo mediaVo = mediaHelper.upload(req.getFile().getContentType(), mediaUrl);

            EvereadingVo evereadingVo = new EvereadingVo();
            evereadingVo.setId(req.getId());
            EvereadingVo result = evereadingMapper.get(evereadingVo);

            UpdateMediaVo updateMediaVo = new UpdateMediaVo();
            updateMediaVo.setId(result.getId());
            updateMediaVo.setMediaId(mediaVo.getId());
            updateMediaVo.setType(req.getType());
            evereadingMapper.updateImage(updateMediaVo);

            switch (req.getType()) {
                case THUMBNAIL:
                    if (result.getThumbnail() != null) mediaHelper.delete(result.getThumbnail());
                    break;
                case INTRO_BANNER:
                    if (result.getIntroBanner() != null) mediaHelper.delete(result.getIntroBanner());
                    break;
                case BANNER:
                    if (result.getBanner() != null) mediaHelper.delete(result.getBanner());
                    break;
                default:
                    break;
            }

            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), "success");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            if (mediaUrl != null) mediaServer.delete(mediaUrl);
            throw new CustomInternalException(StatusCode.DB_UPLOAD_FAIL.getCode(), StatusCode.DB_UPLOAD_FAIL.getMessage(), e.getMessage());
        }
    }
}
