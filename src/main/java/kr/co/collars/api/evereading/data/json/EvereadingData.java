package kr.co.collars.api.evereading.data.json;

import java.util.List;

@lombok.Data
public class EvereadingData {
    private Data eng;
    private Data kor;
    private List<Vocabulary> vocabulary;
}
