package kr.co.collars.api.evereading.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class UploadFileReq {
    private String name;
    @NotNull
    private String sectorKor;
    @NotNull
    private String sectorEng;
    @NotNull
    private float lev;
    @NotNull
    private MultipartFile file;
    private List<String> tags;
}
