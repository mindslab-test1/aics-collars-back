package kr.co.collars.api.evereading.controller;

import kr.co.collars.api.evereading.model.GetDataListReq;
import kr.co.collars.api.evereading.model.GetSectorsReq;
import kr.co.collars.api.evereading.model.UploadDataReq;
import kr.co.collars.api.evereading.service.EvereadingService;
import kr.co.collars.common.CommonRes;
import kr.co.collars.common.StatusCode;
import kr.co.collars.common.exception.CustomInternalException;
import kr.co.collars.common.model.GetById;
import kr.co.collars.common.model.UpdateImageReq;
import kr.co.collars.utils.exception.ValidRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static kr.co.collars.common.Types.*;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/evereading")
public class EvereadingController {
    private final EvereadingService evereadingService;
    private final ValidRequest validRequest;

    // CREATE
    @PostMapping("/data:upload")
    public CommonRes uploadData(
            @Valid @RequestBody UploadDataReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return evereadingService.uploadData(req);
    }

    //READ
    @PostMapping("/data")
    public CommonRes getData(
            @Valid @RequestBody GetById req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return evereadingService.getData(req);
    }

    @PostMapping("/data:list")
    public CommonRes getDataList(
            @Valid @RequestBody GetDataListReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return evereadingService.getDataList(req);
    }

    @PostMapping("/sectors")
    public CommonRes getSectors(
            @Valid @RequestBody GetSectorsReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return evereadingService.getSectors(req);
    }

    // UPDATE
    @PostMapping("/image:update")
    public CommonRes updateImage(
            @Valid UpdateImageReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        String type = req.getType();
        if (type.equalsIgnoreCase(THUMBNAIL) || type.equalsIgnoreCase(INTRO_BANNER) || type.equalsIgnoreCase(BANNER)) {
            return evereadingService.updateImage(req);
        } else {
            throw new CustomInternalException(StatusCode.INVALID_PARAMETERS.getCode(), StatusCode.INVALID_PARAMETERS.getMessage(), "Type should one of [" + THUMBNAIL + ", " + INTRO_BANNER + ", " + BANNER + "]");
        }
    }
}
