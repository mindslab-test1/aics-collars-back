package kr.co.collars.api.evereading.data.json;

import lombok.Data;

@Data
public class Content {
    private String header;
    private String text;
}
