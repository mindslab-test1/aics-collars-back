package kr.co.collars.api.evereading.model;

import lombok.NoArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Getter
@Setter
public class UpdateImageReq {
    @NotNull
    private Integer targetId;
    @NotNull
    private Integer imageId;
    @NotNull
    private String type;
}
