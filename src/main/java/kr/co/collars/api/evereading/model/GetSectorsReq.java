package kr.co.collars.api.evereading.model;

import lombok.Data;

@Data
public class GetSectorsReq {
    private String lang;
}
