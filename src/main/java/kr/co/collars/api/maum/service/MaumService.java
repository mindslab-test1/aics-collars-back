package kr.co.collars.api.maum.service;

import kr.co.collars.api.maum.model.SpeakingReq;
import kr.co.collars.api.maum.model.TtsReq;
import kr.co.collars.common.CommonRes;
import kr.co.collars.common.StatusCode;
import kr.co.collars.common.exception.CustomInternalException;
import kr.co.collars.utils.maum.MaumServer;
import kr.co.collars.utils.maum.SimilarityServer;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@RequiredArgsConstructor
@Slf4j
@Transactional
@Service
public class MaumService {
    private final MaumServer maumServer;
    private final SimilarityServer similarityServer;

    public byte[] doTts(TtsReq req) {
        try {
            return maumServer.tts(req.getText());
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.MAUM_SERVER_FAIL.getCode(), StatusCode.MAUM_SERVER_FAIL.getMessage(), e.getMessage());
        }
    }

    public CommonRes doSpeaking(SpeakingReq req) {
        try {
            String userAnswer = maumServer.cnnSttDetail(req.getFile());
            similarityServer.setDestination();
            double score = similarityServer.GetScore(userAnswer, req.getText()) * 100;
            return new CommonRes(StatusCode.SUCCESS.getCode(), StatusCode.SUCCESS.getMessage(), (int) score);
        } catch (Exception e) {
            throw new CustomInternalException(StatusCode.MAUM_SERVER_FAIL.getCode(), StatusCode.MAUM_SERVER_FAIL.getMessage(), e.getMessage());
        }
    }
}
