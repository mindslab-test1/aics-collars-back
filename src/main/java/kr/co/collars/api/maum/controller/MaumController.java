package kr.co.collars.api.maum.controller;

import kr.co.collars.api.maum.model.SpeakingReq;
import kr.co.collars.api.maum.model.TtsReq;
import kr.co.collars.api.maum.service.MaumService;
import kr.co.collars.common.CommonRes;
import kr.co.collars.utils.exception.ValidRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/maum")
public class MaumController {
    private final MaumService maumService;
    private final ValidRequest validRequest;

    @PostMapping("/tts")
    public ResponseEntity tts(
            @Valid @RequestBody TtsReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=tts-result")
                .header(HttpHeaders.CONTENT_TYPE, "audio/x-wav")
                .body(maumService.doTts(req));
    }

    @PostMapping("/speaking")
    public CommonRes speaking(
            @Valid SpeakingReq req,
            BindingResult bindingResult
    ) {
        validRequest.check(bindingResult);
        return maumService.doSpeaking(req);
    }
}
