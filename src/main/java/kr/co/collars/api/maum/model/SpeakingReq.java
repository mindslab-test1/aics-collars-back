package kr.co.collars.api.maum.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@NoArgsConstructor
@Getter
@Setter
public class SpeakingReq {
    @NonNull
    private MultipartFile file;
    @NonNull
    private List<String> text;
}
